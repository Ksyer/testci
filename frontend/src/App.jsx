import React, { useContext } from 'react';
import { observer } from 'mobx-react';
import { Router, Switch, Route } from 'react-router-dom';
import { Helmet, HelmetProvider } from 'react-helmet-async';

import StockApp from './pages/StockApp';
import setupRoutingStore from './utils/setupRoutingStore';
import rootStore from './stores/RootStore';
import './App.css';
import IndexPage from './pages';
import ArticleIndex from './pages/ArticleList';
import AdminTool from './pages/AdminTool';
import StatementSearch from './pages/StatementSearch';
import demoPage from './pages/demoPage';

export const history = setupRoutingStore(rootStore.routingStore);
const StoreContext = React.createContext({});
export const useRootStore = () => useContext(StoreContext);

const App = observer(() => (
  <Router history={history}>
    <HelmetProvider>
      <StoreContext.Provider value={rootStore}>
        <Switch>
          <Route path="/" exact component={IndexPage} />
          <Route path="/stock-app/:entityType/:entityId" component={StockApp} />
          <Route path="/stock-articles" component={ArticleIndex} />
          <Route path="/admin-tool/:appId" component={AdminTool} />
          <Route path="/statement-search" component={StatementSearch} />
          <Route path="/demo" component={demoPage} />
          <Route>
            <Helmet>
              <title>404 Not Found</title>
            </Helmet>
            <div className="App">
              <p>404 Page not found.</p>
            </div>
          </Route>
        </Switch>
      </StoreContext.Provider>
    </HelmetProvider>
  </Router>
));

export default App;
