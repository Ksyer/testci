import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class ArticleSearchApi extends BaseApi {
  constructor() {
    super(new URL('/api/stock-apps/article-search/', serverConfig.baseUrl));
  }

  _parse({ response }) {
    const { data } = response;
    return data;
  }
}

export const articleSearchApiInstance = new ArticleSearchApi();

export default ArticleSearchApi;
