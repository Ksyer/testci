import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class BlockSecuritiesApi extends BaseApi {
  _parse({ response }) {
    const { data } = response;
    return data.map((obj, i) => ({
      stockInfo: {
        code: obj.code,
        symbol: obj.symbol,
        displayName: obj.display_name,
        name: obj.name,
      },
      peerFinanceData: {
        rank: i + 1,
        ...obj,
      },
    }));
  }

  constructor() {
    super(new URL('api/stock-apps/block-securities/', serverConfig.baseUrl));
  }
}

export const blockSecuritiesApiInstance = new BlockSecuritiesApi();

export default BlockSecuritiesApi;
