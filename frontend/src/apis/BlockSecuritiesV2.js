import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class BlockSecuritiesV2Api extends BaseApi {
  _parse({ response }) {
    const { data } = response;
    return data.map((obj, i) => ({
      stockInfo: {
        code: obj.code,
        symbol: obj.symbol,
        displayName: obj.display_name,
        name: obj.name,
      },
      peerFinanceData: {
        rank: i + 1,
        ...obj,
      },
    }));
  }

  constructor() {
    super(new URL('/api/stock-apps/block-securities-v2/', serverConfig.baseUrl));
  }
}

export const blockSecuritiesV2ApiInstance = new BlockSecuritiesV2Api();

export default BlockSecuritiesV2Api;
