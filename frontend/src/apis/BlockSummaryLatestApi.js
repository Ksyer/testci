import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class BlockSummaryLatestApi extends BaseApi {
  constructor() {
    super(new URL('api/stock-apps/block-summary-latest/', serverConfig.baseUrl));
  }
}

export const blockSummaryLatestApiInstance = new BlockSummaryLatestApi();

export default BlockSummaryLatestApi;
