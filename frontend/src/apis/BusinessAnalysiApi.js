import { serverConfig } from '../config';
import axios from 'axios';

class BusinessAnanysisApi {
  constructor() {
    this.url = new URL('/api/stock-apps/business-analysis/', serverConfig.baseUrl);
  }

  async _get({ stockId }) {
    const params = { stock_id: stockId };
    return await axios.get(this.url.toString(), { params });
  }

  _parse({ response }) {
    const { data: responseData } = response;
    const { data } = responseData;
    const { date_list: dateList, ...rest } = data;
    return {
      dateList,
      ...rest,
    };
  }

  async request({ stockId }) {
    const response = await this._get({ stockId });
    return this._parse({ response });
  }
}

export const businessAnalysisApiInstance = new BusinessAnanysisApi();

export default BusinessAnanysisApi;
