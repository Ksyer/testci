import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class RelatedBlocksApi extends BaseApi {
  constructor() {
    super(new URL('/api/stock-apps/security-blocks/', serverConfig.baseUrl));
  }
}

export const relatedBlocksApiInstance = new RelatedBlocksApi();

export default RelatedBlocksApi;
