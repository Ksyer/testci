import { serverConfig } from '../config';
import BaseApi from './BaseApi';

class WechatRecommendationApi extends BaseApi {
  constructor() {
    super(new URL('/api/stock-apps/wechat-recommendation-v2/', serverConfig.baseUrl));
  }

  _parse({ response }) {
    const { data: responseData } = response;
    const { results } = responseData;
    return results;
  }
}

class WechatRecommendationPaginatedApi extends BaseApi {
  constructor() {
    super(new URL('/api/stock-apps/wechat-recommendation-v2/', serverConfig.baseUrl));
  }

  _parse({ response }) {
    const { data: responseData } = response;
    return responseData;
  }
}

export const wechatRecommendationPaginatedApiInstance = new WechatRecommendationPaginatedApi();

export const wechatRecommendationApiInstance = new WechatRecommendationApi();

export default WechatRecommendationApi;
