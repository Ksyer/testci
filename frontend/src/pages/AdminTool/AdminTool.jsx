import React, { useEffect, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useParams } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { history } from '../../App';
import Layout from '../../components/Layout/Layout';
import ArticleTable from './ArticleAdmin/ArticleTable';
import ExcelUpload from './ExcelUpload/ExcelUpload';
import ConceptList from './ConceptList/ConceptList';
import HotList from './HotList/HotList';
import IndustryList from './IndustryList/IndustryList';
import ManualArticle from './ManualArticle/ManualArticle';
import NegtiveList from './NegtiveList/NegtiveList';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Admin = observer(() => {
  // const hotW = new StoreHotWord();
  const { appId } = useParams();
  const classes = useStyles();

  const handleRowClick = useCallback((e) => {
    const appId = e.currentTarget.getAttribute('value');
    history.push('/admin-tool/' + appId);
  });

  return (
    <Layout>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            {[
              { text: '文章数量监控', key: 'article' },
              { text: '概念词群列表', key: 'concept' },
              { text: '热点词列表', key: 'hot' },
              { text: '行业同义词列表', key: 'industry' },
              { text: '负面词列表', key: 'negtive' },
            ].map((obj, index) => (
              <ListItem button key={obj.key} value={obj.key} onClick={handleRowClick}>
                <ListItemText primary={obj.text} />
              </ListItem>
            ))}
          </List>
          <Divider />
          <List>
            {/* {['手动添加文章', '导入行业Excel'].map((text, index) => (
              <ListItem button key={text}>
                <ListItemText primary={text} />
              </ListItem>
            ))} */}
            {[
              { text: '手动添加文章', key: 'add' },
              { text: '导入行业Excel', key: 'excel' },
              { text: '手动添加券商和公众号', key: 'publisher' },
            ].map((obj, index) => (
              <ListItem button key={obj.key} value={obj.key} onClick={handleRowClick}>
                <ListItemText primary={obj.text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        {appId === 'article' && <ArticleTable />}
        {appId === 'concept' && <ConceptList />}
        {appId === 'hot' && <HotList />}
        {appId === 'industry' && <IndustryList />}
        {appId === 'negtive' && <NegtiveList />}
        {appId === 'manual' && <ManualArticle />}
        {appId === 'excel' && <ExcelUpload />}
        {appId === 'publisher' && <AddPublisher />}
      </main>
    </Layout>
  );
});
export default Admin;
