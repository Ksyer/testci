import React, { Component } from 'react';
import { render } from 'react-dom';
import { serverConfig } from '../../../../src/config';
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '50%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
export default function FormUpload(submit) {
  submit = (e) => {
    e.preventDefault();
    let formData = new FormData(e.target);
    let url = new URL('/api/stock-apps/upload-excel/', serverConfig.baseUrl);
    fetch(url, {
      method: 'POST',
      body: formData,
    })
      .then(function (response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then(function (response) {
        console.log(' 正在上传excel文件!');
      })
      .catch(function (error) {
        console.log('上传excel文件失败！');
      });
  };
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    document.getElementById('btnSave').setAttribute('disabled', true);
    document.getElementById('btnSave').style.backgroundColor = '#555555';
    setOpen(true);
  };
  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    document.getElementById('btnSave').removeAttribute('disabled');
    document.getElementById('btnSave').style.backgroundColor = '#36c';
    setOpen(false);
  };

  return (
    <div>
      <form onSubmit={submit}>
        <input type="file" name="file" id="file" accept=".xlsx,.xls" />
        <input type="submit" value="提交" onclick={handleClick} id="btnSave" />
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="info">
            正在上传excel文件!
          </Alert>
        </Snackbar>
      </form>
    </div>
  );
}
