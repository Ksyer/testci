import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import { action, observable, runInAction } from 'mobx';
import { Grid, Table, Paper, TableBody, TableContainer, TableHead } from '@material-ui/core';
import { hotWordsApi } from '../../../apis/HotWordApi';
import { StyledTableCell, StyledTableRow } from '../ArticleAdmin/ArticleTable';

const SquarePaper = (props) => <Paper square {...props} />;

export class StoreHotWord {
  @observable hotWords = null;

  @action.bound
  getHotWords() {
    return this.apis.HotWordApi.request()
      .then((data) => {
        runInAction(() => {
          this.hotWords = data;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        runInAction(() => {
          console.log(err);
          console.log('Wrong HotWords');
        });
      });
  }

  constructor() {
    this.apis = {
      HotWordApi: hotWordsApi,
    };
  }
}
const HotList = observer(() => {
  const hotW = new StoreHotWord();

  useEffect(() => {
    hotW.getHotWords();
  }, []);

  return <HotListTable hotW={hotW} />;
});

const HotListTable = observer(({ hotW }) => {
  const data = hotW.hotWords;
  const config = {
    cols: ['date', 'rank', 'idf', 'word', 'title'],
    colNameMap: {
      date: '日期',
      rank: '排序',
      idf: 'idf',
      word: '热词',
      title: '文章',
    },
    defaultFormatter: 'identity',
    colFormatter: {},
  };
  const cols = config.cols;
  const colNameMap = config.colNameMap;
  const keyFunc = (obj) => obj.date + obj.rank;
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TableContainer component={SquarePaper}>
          <Table className={'hotWordList'}>
            <TableHead>
              <StyledTableRow>
                {cols.map((colKey) => (
                  <StyledTableCell key={colKey} align="center">
                    {colNameMap[colKey]}
                  </StyledTableCell>
                ))}
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {data != null &&
                data.map((obj) => (
                  <StyledTableRow key={keyFunc(obj)}>
                    <StyledTableCell align="center">{obj.date}</StyledTableCell>
                    <StyledTableCell align="center">{obj.rank}</StyledTableCell>
                    <StyledTableCell align="center">{obj.idf}</StyledTableCell>
                    <StyledTableCell align="center">{obj.word}</StyledTableCell>
                    <StyledTableCell align="center">
                      {obj.article_idf.map(function (a_idf, index) {
                        if (a_idf) {
                          return (
                            <div>
                              <a href={obj.url[index]} target="_blank;">
                                文章（{index + 1}）：{obj.title[index]} —（{obj.article_idf[index]}
                                ）
                              </a>
                            </div>
                          );
                        }
                      })}
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
});

export default HotList;
