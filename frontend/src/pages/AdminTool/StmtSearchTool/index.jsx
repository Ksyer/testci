import React, { useEffect, useCallback } from 'react';
import { Helmet } from 'react-helmet-async';
import {
  Button,
  Grid,
  Input,
  Paper,
  Table,
  TableHead,
  TableFooter,
  TableRow,
  TableCell,
  TableContainer,
  TablePagination,
  TextField,
  Typography,
} from '@material-ui/core';
import { observer } from 'mobx-react';
import { format, isValid } from 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';

import { statementSearchConfig } from '../../../consts/displayConfig';
import { StoreProvider, useStore } from './store';
import useStyles from './useStyles';
import Layout from '../../../components/Layout/Layout';
import RealArticleTableBody from '../views/RealArticleTableBody';
import SkeletonArticleTableBody from '../views/SkeletonArticalTableBody';

const appName = '研报段落搜索工具';

const SquarePaper = (props) => <Paper square {...props} />;

const ArticleView = observer(() => {
  const { cols, colNameMap, defaultFormatter, colFormatterMap } = statementSearchConfig;
  const store = useStore();
  const classes = useStyles();
  const {
    articleList,
    getArticles,
    loading,
    pageSize,
    totalCount,
    page,
    setPage,
    filterStore: { setValue, handleSubmit, fieldState, fieldError, changed },
  } = store;
  const dataReady = articleList != null;
  const handleChangePage = useCallback((e, page) => {
    // setPage({ page: page + 1 });
  }, []);
  const handleChangeField = useCallback((e) => {
    const key = e.target.id;
    setValue(key, e.target.value);
  }, []);
  const handleFormSubmit = useCallback((e) => {
    e.preventDefault();
    handleSubmit();
  });
  useEffect(() => {}, []);
  return (
    <Layout>
      <Helmet>
        <title>{appName}</title>
      </Helmet>
      <Grid container spacing={3} className={classes.wrap}>
        <Grid item xs={12}>
          <Typography variant="h5">{appName}</Typography>
        </Grid>
        <Grid container item xs={12} justify="space-between" component={SquarePaper}>
          <Grid container component="form" spacing={1} onSubmit={handleFormSubmit}>
            <Grid container item xs={12}>
              <Grid item xs={8}>
                <Grid container spacing={1}>
                  <Grid item>
                    <TextField
                      id="statements"
                      label="段落搜索"
                      className={classes.searchInput}
                      value={fieldState.statements}
                      onChange={handleChangeField}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid container item xs={12} justify="space-between" alignContent="center">
              <Typography color="error" className={classes.centerText}>
                {changed ? '条件已改变，请重新提交表单' : ''}
              </Typography>
              <Button
                variant="contained"
                color="primary"
                className={classes.searchButton}
                type="submit"
              >
                搜索
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid container item xs={12} />
        <TableContainer component={SquarePaper}>
          <Table size="small" className={classes.tableCompact}>
            <TableHead>
              <TableRow>
                {dataReady && (
                  <TablePagination
                    count={totalCount}
                    page={page - 1}
                    rowsPerPage={pageSize}
                    rowsPerPageOptions={[pageSize]}
                  />
                )}
              </TableRow>
              {dataReady && <TableRow></TableRow>}
            </TableHead>
            {dataReady ? (
              <RealArticleTableBody
                data={articleList}
                cols={cols}
                defaultFormatter={defaultFormatter}
                colFormatterMap={colFormatterMap}
                loading={loading}
                keywords={[fieldState['statements']]}
              />
            ) : (
              <SkeletonArticleTableBody cols={cols} />
            )}
            {/* <TableFooter>
              {dataReady && (
                <TableRow>
                  <TablePagination
                    count={totalCount}
                    onChangePage={handleChangePage}
                    page={page - 1}
                    rowsPerPage={pageSize}
                    rowsPerPageOptions={[pageSize]}
                  />
                </TableRow>
              )}
            </TableFooter> */}
          </Table>
        </TableContainer>
      </Grid>
    </Layout>
  );
});

const ArticleIndex = observer(() => {
  return (
    <StoreProvider>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <ArticleView />
      </MuiPickersUtilsProvider>
    </StoreProvider>
  );
});

export default ArticleIndex;
