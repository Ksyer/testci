import React from 'react';
import { action, observable, runInAction } from 'mobx';
import { format, addDays, addMonths, isValid } from 'date-fns';
import uuid from 'node-uuid';

import { statementSearchPaginatedApiInstance } from '../../../apis/StatementSearchApi';

class FilterStore {
  stateList = ['fromDate', 'toDate', 'displayName', 'title', 'author', 'statements', 'score'];

  @observable fieldState = {
    fromDate: null,
    toDate: null,
    displayName: '',
    title: '',
    author: '',
    statements: '',
    score: null,
  };

  @observable prevState = {
    fromDate: null,
    toDate: null,
    displayName: '',
    title: '',
    author: '',
    statements: '',
    score: null,
  };

  @observable fieldError = {};

  @observable changed = false;

  @action.bound storePrevState() {
    this.prevState = { ...this.fieldState };
  }

  // parse state into request params
  @action.bound parseParams() {
    const params = {};
    if (this.fieldState.fromDate != null) {
      params.fromDate = format(this.fieldState.fromDate, 'yyyy-MM-dd');
    }
    if (this.fieldState.toDate != null) {
      params.toDate = format(addDays(this.fieldState.toDate, 1), 'yyyy-MM-dd');
    }
    if (this.fieldState.displayName != '') {
      params.displayNameLike = this.fieldState.displayName;
    }
    if (this.fieldState.title != '') {
      params.titleLike = this.fieldState.title;
    }
    if (this.fieldState.author != '') {
      params.authorLike = this.fieldState.author;
    }
    if (this.fieldState.statements != '') {
      params.statementsLike = this.fieldState.statements;
    }
    if (this.fieldState.score != '') {
      params.score = this.fieldState.score;
    }
    return params;
  }

  @action.bound setValue(key, value, validate = true) {
    this.fieldState[key] = value;
    if (validate) {
      this.changed = true;
      if (this.fieldState[key] === this.prevState[key]) {
        this.changed = false;
        this.stateList.forEach((key) => {
          if (this.fieldState[key] !== this.prevState[key]) {
            this.changed = true;
          }
        });
      }
      this.handleValidate();
    }
  }

  @action.bound getValue(key) {
    return this.fieldState[key];
  }

  // handleSubmit make query from fieldState
  // only proceed with submit if there is no error.
  @action.bound handleSubmit() {
    const errorFlag = this.handleValidate();
    if (!errorFlag) {
      this.store.getArticles({ page: 1 });
    }
  }

  // handleValidate validate fieldState and update fieldError
  @action.bound handleValidate() {
    let flag = false;
    this.fieldError = {};
    if (!isValid(this.fieldState.fromDate)) {
      this.fieldError.fromDate = '日期不合法';
      flag = true;
    }
    if (!isValid(this.fieldState.toDate)) {
      this.fieldError.toDate = '日期不合法';
      flag = true;
    }
    if (
      isValid(this.fieldState.fromDate) &&
      isValid(this.fieldState.toDate) &&
      this.fieldState.fromDate > this.fieldState.toDate
    ) {
      this.fieldError.fromDate = '开始日期须在截止日期之前';
      this.fieldError.toDate = '开始日期须在截止日期之前';
      flag = true;
    }
    return flag;
  }

  constructor(store) {
    this.store = store;
  }
}

class Store {
  @observable pageSize = 100;

  @observable articleList = null;

  @observable loading = false;

  @observable page = 1;

  @observable totalCount = null;

  signature = null;

  @action.bound
  setPage({ page }) {
    if (this.page == page) {
      return;
    }
    this.getArticles({ page });
  }

  @action.bound
  getArticles({ page = 1 }) {
    this.articleList = [];
    const params = { page, pageSize: this.pageSize, ...this.filterStore.parseParams() };
    params.statementsLike = params.statementsLike.replace(/(^\s*)|(\s*$)/g, '');
    if (params.statementsLike == undefined) {
      console.log('没有筛选');
      return;
    }
    this.filterStore.storePrevState();
    this.filterStore.changed = false;
    this.loading = true;
    const signature = uuid.v4();
    this.signature = signature;
    this.apis.statementSearch
      .request(params)
      .then((data) => {
        if (this.signature === signature) {
          runInAction(() => {
            this.articleList = data.results;
            this.totalCount = data.count;
            this.page = page;
            this.loading = false;
          });
        }
      })
      .catch(() => {
        runInAction(() => {
          this.articleList = [];
          this.loading = false;
        });
      });
  }

  dispose() {
    this.articleList = [];
    this.page = 1;
    this.totalCount = null;
    this.fromDate = null;
    this.toDate = null;
  }

  constructor() {
    this.apis = {
      statementSearch: statementSearchPaginatedApiInstance,
    };
    this.filterStore = new FilterStore(this);
    this.filterStore.setValue('fromDate', addMonths(new Date(), -3), false);
    this.filterStore.setValue('toDate', new Date(), false);
  }
}

const storeInstance = new Store();

const storeContext = React.createContext(null);

export const StoreProvider = ({ children }) => (
  <storeContext.Provider value={storeInstance}>{children}</storeContext.Provider>
);

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) throw new Error('useStore must be used within a StoreProvider.');
  return store;
};

export default Store;
