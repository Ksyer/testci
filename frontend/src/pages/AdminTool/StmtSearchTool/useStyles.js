import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  hoverCell: {
    '&:hover': {
      backgroundColor: '#f0f0f0',
    },
  },
  tabs: {
    border: '1px solid #c0c0c0',
  },
  tab: {
    width: 90,
    minWidth: 'unset',
  },
  tableCompact: {
    '& th, & td': {
      padding: [[2, 4, 2, 4]],
    },
  },
  row: {
    '&:hover': {
      backgroundColor: '#f0f0f0',
    },
  },
  rowFocused: {
    backgroundColor: 'rgba(252, 230, 100, 0.5)',
  },
  rowImportant: {
    '& td': {
      color: theme.palette.primary.main,
    },
  },
  keyRow: {
    backgroundColor: '#e0e0e0',
  },
  fakeRow: {
    padding: [[12, 16, 12, 4]],
  },
  clickable: {
    cursor: 'pointer',
  },
  graphTitleInner: {
    position: 'absolute',
    top: 16,
    left: 16,
  },
  graphFrame: {
    width: '100%',
    paddingTop: '30%',
    position: 'relative',
  },
  graphInner: {
    backgroundColor: 'rgba(25, 235, 220, 0.2)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  wrap: {
    paddingTop: 16,
  },
  subtitle: {
    verticalAlign: 'bottom',
  },
  seperatorCell: {
    paddingTop: [[theme.spacing(1.5)], '!important'],
    paddingBottom: [[theme.spacing(1.5)], '!important'],
  },
  fixedCell20: {
    width: '20%',
  },
  fixedCell40: {
    width: '40%',
    paddingTop: '10px !important',
    paddingBottom: '10px !important',
    '& div': {
      display: '-webkit-box',
      '-webkit-line-clamp': 6,
      '-webkit-box-orient': 'vertical',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  },
  fixedCell80: {
    width: '80%',
    paddingTop: '10px !important',
    paddingBottom: '10px !important',
    '& div': {
      display: '-webkit-box',
      '-webkit-line-clamp': 6,
      '-webkit-box-orient': 'vertical',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  },
  rightSideCell: {
    textAlign: 'right',
  },
  tableInput: {
    textAlign: 'center',
    '&::placeholder': {
      opacity: 0.5,
    },
  },
  inlineSkeleton: {
    transformOrigin: [[0, '50%']],
  },
  loadingCell: {
    color: '#777 !important',
  },
  textCell: {
    textAlign: 'center',
  },
  hidden: { visibility: 'collapse' },
  appNameButton: {
    width: '6rem',
  },
  showLineBreak: {
    whiteSpace: 'pre-line',
  },
  pickerFrame: {
    display: 'inline-block',
    width: 160,
    paddingLeft: 8,
  },
  searchTitle: {
    verticalAlign: 'middle',
  },
  searchInput: {
    width: '11rem',
  },
  searchButton: {
    width: '10rem',
  },
  centerText: {
    margin: 'auto 0',
  },
}));

export default useStyles;
