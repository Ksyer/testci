import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Grid, Typography } from '@material-ui/core';
import { observer } from 'mobx-react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import { StoreProvider, useStore } from './store';
import useStyles from './useStyles';
import Layout from '../../components/Layout/Layout';
import ArticleTable from './views/ArticleTable';
import { articleSearchConfig, wechatRecommendationConfig } from '../../consts/displayConfig';

const appName = '头条文章总览';

const ArticleView = observer(() => {
  const store = useStore();
  const classes = useStyles();
  const { getArticles } = store;
  useEffect(() => {
    getArticles({
      page: 1,
    });
  }, []);
  return (
    <Layout>
      <Helmet>
        <title>{appName}</title>
      </Helmet>
      <Grid container spacing={3} className={classes.wrap}>
        <Grid item xs={12}>
          <Typography variant="h5">{appName}</Typography>
        </Grid>
      </Grid>
      <ArticleTable title={appName} store={store} config={articleSearchConfig} />
    </Layout>
  );
});

const ArticleIndex = observer(() => {
  return (
    <StoreProvider>
      <ArticleView />
    </StoreProvider>
  );
});

export default ArticleIndex;
