/*
[Deprecated] Legacy ArticleStore for WechatRecommendation

Legacy ArticleStore that preserves original getArticles behavior.
This store should be used solely for compatibility reasons. It will
be removed in a future version.
*/
import React from 'react';
import { action, runInAction } from 'mobx';
import uuid from 'node-uuid';

import Store from './store';

class RecommendationStore extends Store {
  @action.bound
  getArticles({ page = 1, state }) {
    this.focusedRow = -1;
    if (state != null) {
      Object.keys(state).forEach((key) => {
        this.filterStore.setValue(key, state[key]);
      });
    }
    const params = { page, pageSize: this.pageSize, ...this.filterStore.parseParams() };
    this.filterStore.storePrevState();
    this.filterStore.changed = false;
    this.loading = true;
    const signature = uuid.v4();
    this.signature = signature;
    return this.apis.wechatRecommendation
      .request(params)
      .then((data) => {
        if (this.signature === signature) {
          runInAction(() => {
            this.articleList = data.results.map((obj) => ({
              ...obj,
              keywords: [obj.display_name],
              authorUpdateTime: `${obj.author}\n${obj.updatetime}`,
              related_stocks: [
                [`#${obj.a_code}${obj.display_name}`, `/stock-app/stocks/${obj.a_code}/info`],
              ],
              // display_name: [obj.display_name, `/stock-app/stocks/${obj.a_code}/info`],
              title: [obj.title, obj.url],
            }));
            this.totalCount = data.count;
            this.page = page;
            this.loading = false;
          });
        }
      })
      .catch(() => {
        runInAction(() => {
          this.articleList = [];
          this.loading = false;
        });
      });
  }
}

const storeInstance = new RecommendationStore();

const storeContext = React.createContext(null);

export const StoreProvider = ({ children }) => (
  <storeContext.Provider value={storeInstance}>{children}</storeContext.Provider>
);

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) throw new Error('useStore must be used within a StoreProvider.');
  return store;
};

export default RecommendationStore;
