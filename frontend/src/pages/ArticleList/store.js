import React from 'react';
import { action, computed, observable, runInAction } from 'mobx';
import { format, addDays, addMonths, isValid } from 'date-fns';
import uuid from 'node-uuid';

import { wechatRecommendationPaginatedApiInstance } from '../../apis/WechatRecommendationApi';
import { articleSearchApiInstance } from '../../apis/ArticleSearchApi';

class FilterStore {
  fieldList = [
    'fromDate',
    'toDate',
    'displayName',
    'title',
    'author',
    'statements',
    'score',
    'stock_id',
  ];

  @observable fieldState = {
    fromDate: null,
    toDate: null,
    displayName: '',
    title: '',
    author: '',
    statements: '',
    score: '',
    stockId: '',
  };

  @observable prevState = {
    fromDate: null,
    toDate: null,
    displayName: '',
    title: '',
    author: '',
    statements: '',
    score: '',
    stockId: '',
  };

  @observable fieldError = {};

  // `changed` parameter checks if state is changed but not submitted
  // If `changed` is true, view component should prompt user to resubmit form
  @observable changed = false;

  @action.bound storePrevState() {
    this.prevState = { ...this.fieldState };
  }

  // parse state into request params
  @action.bound parseParams() {
    const params = {};
    if (this.fieldState.fromDate != null) {
      params.fromDate = format(this.fieldState.fromDate, 'yyyy-MM-dd');
    }
    if (this.fieldState.toDate != null) {
      params.toDate = format(addDays(this.fieldState.toDate, 1), 'yyyy-MM-dd');
    }
    if (this.fieldState.displayName != '') {
      params.displayNameLike = this.fieldState.displayName;
    }
    if (this.fieldState.title != '') {
      params.titleLike = this.fieldState.title;
    }
    if (this.fieldState.author != '') {
      params.authorLike = this.fieldState.author;
    }
    if (this.fieldState.statements != '') {
      params.statementsLike = this.fieldState.statements;
    }
    if (this.fieldState.score != '') {
      params.score = this.fieldState.score;
    }
    if (this.fieldState.stockId != '') {
      params.stockId = this.fieldState.stockId;
    }
    return params;
  }

  @action.bound setValue(key, value, validate = true) {
    this.fieldState[key] = value;
    if (validate) {
      this.changed = true;
      if (this.fieldState[key] === this.prevState[key]) {
        this.changed = false;
        this.fieldList.forEach((key) => {
          if (this.fieldState[key] !== this.prevState[key]) {
            this.changed = true;
          }
        });
      }
      this.handleValidate();
    }
  }

  @action.bound getValue(key) {
    return this.fieldState[key];
  }

  // handleSubmit make query from fieldState
  // only proceed with submit if there is no error.
  @action.bound handleSubmit() {
    const errorFlag = this.handleValidate();
    if (!errorFlag) {
      this.store.getArticles({ page: 1 });
    }
  }

  // handleValidate validate fieldState and update fieldError
  @action.bound handleValidate() {
    let flag = false;
    this.fieldError = {};
    if (!isValid(this.fieldState.fromDate)) {
      this.fieldError.fromDate = '日期不合法';
      flag = true;
    }
    if (!isValid(this.fieldState.toDate)) {
      this.fieldError.toDate = '日期不合法';
      flag = true;
    }
    if (
      isValid(this.fieldState.fromDate) &&
      isValid(this.fieldState.toDate) &&
      this.fieldState.fromDate > this.fieldState.toDate
    ) {
      this.fieldError.fromDate = '开始日期须在截止日期之前';
      this.fieldError.toDate = '开始日期须在截止日期之前';
      flag = true;
    }
    return flag;
  }

  constructor(store) {
    this.store = store;
  }
}

class Store {
  @observable pageSize = 15;

  @observable articleList = null;

  @observable loading = false;

  @observable page = 1;

  @observable totalCount = null;

  @observable focusedRow = -1;

  signature = null;

  @computed get colKeywords() {
    if (this.filterStore == null || this.filterStore.fieldState == null) {
      return {};
    }
    return this.filterStore.fieldList.reduce(
      (agg, colKey) => ({
        ...agg,
        [colKey]: [this.filterStore.fieldState[colKey]],
      }),
      {}
    );
  }

  @action.bound
  setPage({ page }) {
    if (this.page == page) {
      return;
    }
    this.getArticles({ page });
  }

  @action.bound
  getArticles({ page = 1, state }) {
    this.focusedRow = -1;
    if (state != null) {
      Object.keys(state).forEach((key) => {
        this.filterStore.setValue(key, state[key]);
      });
    }
    const params = { page, pageSize: this.pageSize, ...this.filterStore.parseParams() };
    this.filterStore.storePrevState();
    this.filterStore.changed = false;
    this.loading = true;
    const signature = uuid.v4();
    this.signature = signature;
    return this.apis.articleSearch
      .request(params)
      .then((data) => {
        if (this.signature !== signature) {
          return;
        }
        console.log(data);
        runInAction(() => {
          console.log('setting articleList');
          this.articleList = data.results.map((obj) => ({
            authorUpdateTime: `${obj.article_author}\n${obj.article_updatetime}`,
            title: [obj.article_title, obj.article_url],
            relatedStocks: obj.related_stock_list.map((stockObj) => [
              `#${stockObj.a_code}${stockObj.display_name}`,
              `/stock-app/stocks/${stockObj.a_code}/info`,
            ]),
            stockScore: obj.related_stock_list.map((stockObj) => stockObj.score).join('__sep__'),
            statements: obj.statement_list.map((statementObj) => statementObj.text).join('__sep__'),
            detailList: obj.related_stock_list.map((stockObj) => [
              `#${stockObj.a_code}${stockObj.display_name}★${stockObj.score}`,
              `/stock-app/stocks/${stockObj.a_code}/info`,
            ]),
            keywords: obj.related_stock_list.map((stockObj) => stockObj.display_name),
          }));
          this.totalCount = data.count;
          this.page = page;
          this.loading = false;
        });
      })
      .catch((e) => {
        console.error(e);
        runInAction(() => {
          this.articleList = [];
          this.loading = false;
        });
      });
  }

  @action.bound
  handleChangePage(e, page) {
    this.setPage({ page: page + 1 });
  }

  @action.bound
  handleChangeFromDate(date) {
    this.filterStore.setValue('fromDate', date);
  }

  @action.bound
  handleChangeToDate(date) {
    this.filterStore.setValue('toDate', date);
  }

  @action.bound
  handleChangeField(e) {
    const key = e.target.id;
    this.filterStore.setValue(key, e.target.value);
  }

  @action.bound
  handleFormSubmit(e) {
    e.preventDefault();
    this.filterStore.handleSubmit();
  }

  @action.bound
  toggleFocusedRow(rowIdx) {
    if (this.focusedRow === rowIdx) {
      this.focusedRow = -1;
      return;
    }
    this.focusedRow = rowIdx;
  }

  dispose() {
    this.articleList = [];
    this.page = 1;
    this.totalCount = null;
    this.fromDate = null;
    this.toDate = null;
  }

  constructor() {
    this.apis = {
      wechatRecommendation: wechatRecommendationPaginatedApiInstance,
      articleSearch: articleSearchApiInstance,
    };
    this.filterStore = new FilterStore(this);
    this.filterStore.setValue('fromDate', addMonths(new Date(), -6), false);
    this.filterStore.setValue('toDate', new Date(), false);
  }
}

const storeInstance = new Store();

const storeContext = React.createContext(null);

export const StoreProvider = ({ children }) => (
  <storeContext.Provider value={storeInstance}>{children}</storeContext.Provider>
);

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) throw new Error('useStore must be used within a StoreProvider.');
  return store;
};

export default Store;
