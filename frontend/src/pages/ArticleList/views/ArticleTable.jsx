import React from 'react';
import {
  Button,
  Grid,
  Input,
  Table,
  TableHead,
  TableFooter,
  TableRow,
  TableCell,
  TableContainer,
  TablePagination,
  Typography,
  Paper,
} from '@material-ui/core';
import { observer } from 'mobx-react';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';

import useStyles from '../useStyles';
import RealArticleTableBody from '../views/RealArticleTableBody';
import SkeletonArticleTableBody from '../views/SkeletonArticalTableBody';
import DateFnsUtils from '@date-io/date-fns';

const SquarePaper = (props) => <Paper square {...props} />;

const ArticleTableInner = observer(({ title, store, config }) => {
  const {
    cols,
    colNameMap,
    searchNameMap,
    searchValueMap,
    defaultFormatter,
    colFormatterMap,
    cellTypeMap,
    manageSearch,
  } = config;
  const classes = useStyles();
  const {
    articleList,
    loading,
    pageSize,
    totalCount,
    page,
    handleChangePage,
    handleChangeFromDate,
    handleChangeToDate,
    handleChangeField,
    handleFormSubmit,
    focusedRow,
    toggleFocusedRow,
    colKeywords,
    filterStore: { fieldState, fieldError, changed },
  } = store;
  const dataReady = articleList != null;
  console.log(articleList);
  console.log(totalCount);
  return (
    <TableContainer component={SquarePaper}>
      <form onSubmit={handleFormSubmit}>
        <Table size="small" className={classes.tableCompact}>
          <TableHead>
            <TableRow>
              <TableCell colSpan={cols.length - 1} className={classes.seperatorCell}>
                <Typography variant="body1">{title}</Typography>
              </TableCell>
              {dataReady && (
                <TablePagination
                  count={totalCount}
                  onChangePage={handleChangePage}
                  page={page - 1}
                  rowsPerPage={pageSize}
                  rowsPerPageOptions={[pageSize]}
                />
              )}
            </TableRow>
            {dataReady && <TableRow></TableRow>}
            {(manageSearch == null || manageSearch) && (
              <TableRow>
                <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                  <Grid container justify="space-between">
                    <Grid item>
                      <span className={classes.pickerFrame}>
                        <KeyboardDatePicker
                          disableFuture
                          label="开始日期"
                          variant="inline"
                          format="yyyy-MM-dd"
                          id="date-picker-from"
                          value={fieldState.fromDate}
                          error={fieldError.fromDate != null}
                          helperText={fieldError.fromDate}
                          onChange={handleChangeFromDate}
                        />
                      </span>
                      <span className={classes.pickerFrame}>
                        <KeyboardDatePicker
                          disableFuture
                          label="截止日期"
                          variant="inline"
                          format="yyyy-MM-dd"
                          id="date-picker-from"
                          value={fieldState.toDate}
                          helperText={fieldError.toDate}
                          error={fieldError.toDate != null}
                          onChange={handleChangeToDate}
                        />
                      </span>
                    </Grid>
                    <Grid item xs={6}>
                      <Grid
                        container
                        justify="flex-end"
                        alignContent="center"
                        style={{ height: '100%' }}
                      >
                        <Typography color="error" className={classes.centerText}>
                          {changed ? '条件已改变，请重新提交表单' : ''}
                        </Typography>
                        <div style={{ width: '1rem' }} />
                        <Button
                          variant="contained"
                          color="primary"
                          className={classes.searchButton}
                          type="submit"
                        >
                          筛选
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </TableCell>
              </TableRow>
            )}
            <TableRow>
              {cols.map((colKey) => (
                <TableCell key={colKey} className={classes.textCell}>
                  {manageSearch && searchValueMap[colKey] != null ? (
                    <Input
                      id={searchValueMap[colKey]}
                      placeholder={searchNameMap[colKey]}
                      value={fieldState[searchValueMap[colKey]]}
                      onChange={handleChangeField}
                      inputProps={{ className: classes.tableInput }}
                      fullWidth
                    />
                  ) : (
                    <Typography style={{ whiteSpace: 'nowrap' }}>{colNameMap[colKey]}</Typography>
                  )}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          {dataReady ? (
            <RealArticleTableBody
              data={articleList}
              cols={cols}
              defaultFormatter={defaultFormatter}
              colFormatterMap={colFormatterMap}
              searchValueMap={searchValueMap}
              cellTypeMap={cellTypeMap}
              loading={loading}
              focusedRow={focusedRow}
              toggleFocusedRow={toggleFocusedRow}
              colKeywords={colKeywords}
            />
          ) : (
            <SkeletonArticleTableBody cols={cols} />
          )}
          <TableFooter>
            {dataReady && (
              <TableRow>
                <TablePagination
                  count={totalCount}
                  onChangePage={handleChangePage}
                  page={page - 1}
                  rowsPerPage={pageSize}
                  rowsPerPageOptions={[pageSize]}
                />
              </TableRow>
            )}
          </TableFooter>
        </Table>
      </form>
    </TableContainer>
  );
});

const ArticleTable = observer(({ ...props }) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <ArticleTableInner {...props} />
    </MuiPickersUtilsProvider>
  );
});

export default ArticleTable;
