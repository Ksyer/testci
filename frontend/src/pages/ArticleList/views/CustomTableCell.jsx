import React from 'react';
import { Link as MuiLink, TableCell, Typography } from '@material-ui/core';
import { observer } from 'mobx-react';

import cellFormatter from '../../../utils/cellFormatter';
import HighlightTypography from '../../StockApp/views/components/HighlightTypography';

const entityListMaxLength = 6;

const CustomTableCell = observer((props) => {
  const {
    row,
    colKey,
    cellType,
    // keywords,
    multiLevelKeywords,
    classes,
    loading,
    colFormatterMap,
    defaultFormatter,
  } = props;
  let classNameList = [];
  switch (cellType) {
    case 'fixed-short':
      classNameList.push(classes.fixedCell20);
      break;
    case 'fixed-long':
    case 'bullet-list':
      classNameList.push(classes.fixedCell40);
      break;
    case 'fixed-rem-short':
      classNameList.push(classes.fixedCellEmShort);
      break;
    case 'fixed-rem-long':
      classNameList.push(classes.fixedCellEmLong);
      break;
    case 'link':
      classNameList.push(classes.fixedCellEmLong);
      break;
    case 'link-list-long':
      classNameList.push(classes.fixedCellEmLong);
      break;
    case 'list':
      classNameList.push(classes.fixedCellEmShort);
      break;
    default:
      classNameList.push(classes.fixedCell20);
      break;
  }
  if (loading) {
    classNameList.push(classes.loadingCell);
  }
  const className = classNameList.join(' ');
  const formatterType = colFormatterMap[colKey] || defaultFormatter;
  switch (cellType) {
    case 'link':
      return (
        <TableCell className={className}>
          <MuiLink
            href={row[colKey][1]}
            target="_blank"
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <HighlightTypography multiLevelKeywords={multiLevelKeywords}>
              {cellFormatter(row[colKey][0], formatterType)}
            </HighlightTypography>
          </MuiLink>
        </TableCell>
      );
    case 'link-list':
    case 'link-list-long': {
      const contentList = row[colKey];
      const contentLength = contentList.length;
      return (
        <TableCell className={className}>
          {contentList.slice(0, entityListMaxLength).map((obj) => (
            <MuiLink
              key={obj[0]}
              href={obj[1]}
              target="_blank"
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              <HighlightTypography multiLevelKeywords={multiLevelKeywords}>
                {obj[0]}
              </HighlightTypography>
            </MuiLink>
          ))}
          {contentLength > entityListMaxLength && (
            <Typography>{`...全部 (${contentLength})`}</Typography>
          )}
        </TableCell>
      );
    }
    case 'list': {
      const contentList = row[colKey].split('__sep__');
      const contentLength = contentList.length;
      return (
        <TableCell className={className}>
          {contentList.slice(0, entityListMaxLength).map((part, index) => (
            <div key={index}>
              <HighlightTypography multiLevelKeywords={multiLevelKeywords}>
                {part}
              </HighlightTypography>
            </div>
          ))}
          {contentLength > entityListMaxLength && <div>...</div>}
        </TableCell>
      );
    }
    case 'bullet-list':
    case 'bullet-list-short':
      return (
        <TableCell className={className}>
          <div>
            <ul>
              {row[colKey].split('__sep__').map((part, index) => (
                <li key={index}>
                  <HighlightTypography multiLevelKeywords={multiLevelKeywords}>
                    {part}
                  </HighlightTypography>
                </li>
              ))}
            </ul>
          </div>
        </TableCell>
      );
    default:
      return (
        <TableCell className={className}>
          <HighlightTypography multiLevelKeywords={multiLevelKeywords}>
            {cellFormatter(row[colKey], formatterType)}
          </HighlightTypography>
        </TableCell>
      );
  }
});

export default CustomTableCell;
