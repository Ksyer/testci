import React, { useCallback } from 'react';
import { Link as MuiLink, TableBody, TableRow, TableCell, Collapse } from '@material-ui/core';
import { observer } from 'mobx-react';

import useStyles from '../useStyles';
import CustomTableCell from './CustomTableCell';
import HighlightTypography from '../../StockApp/views/components/HighlightTypography';

const RealArticleTableBody = observer(
  ({
    data,
    cols,
    defaultFormatter,
    colFormatterMap,
    searchValueMap,
    cellTypeMap,
    loading,
    focusedRow,
    toggleFocusedRow,
    colKeywords,
  }) => {
    const classes = useStyles();
    const handleRowClick = useCallback(
      (e) => {
        toggleFocusedRow(Number(e.currentTarget.getAttribute('value')));
      },
      [toggleFocusedRow]
    );
    const _searchValueMap = searchValueMap || {};
    return (
      <TableBody>
        {data.map((row, rowIdx) => (
          <React.Fragment
            key={row.key || row.article_id + row.a_code + row.author + row.updatetime + row.title}
          >
            <TableRow
              className={`${classes.hoverCell} ${classes.clickable}`}
              value={rowIdx}
              onClick={handleRowClick}
            >
              {cols.map((colKey) => (
                <CustomTableCell
                  key={colKey}
                  row={row}
                  colKey={colKey}
                  colFormatterMap={colFormatterMap}
                  defaultFormatter={defaultFormatter}
                  multiLevelKeywords={[
                    colKeywords[_searchValueMap[colKey]] || [],
                    row.keywords || [],
                  ]}
                  cellType={cellTypeMap[colKey] || null}
                  classes={classes}
                  loading={loading}
                />
              ))}
            </TableRow>
            <TableRow
              className={`${classes.hoverCell} ${classes.clickable}`}
              value={rowIdx}
              onClick={handleRowClick}
            >
              <TableCell colSpan={cols.length} className={classes.collapseCell}>
                {/* Collapsable Content */}
                <Collapse in={rowIdx === focusedRow}>
                  <div className={classes.collapseCellContent}>
                    {row['detailList'] &&
                      row['detailList'].map((obj) => (
                        <MuiLink
                          key={obj[0]}
                          href={obj[1]}
                          target="_blank"
                          component="span"
                          className={classes.collapseCellListItem}
                          onClick={(e) => {
                            e.stopPropagation();
                          }}
                        >
                          <HighlightTypography
                            // keywords={row.keywords}
                            multiLevelKeywords={[
                              colKeywords['displayName'] || [],
                              row.keywords || [],
                            ]}
                            component="span"
                          >
                            {obj[0]}
                          </HighlightTypography>
                        </MuiLink>
                      ))}
                    <HighlightTypography color="primary" className={classes.collapseCellTitle}>
                      推荐详情
                    </HighlightTypography>
                    <ul>
                      {row['statements'].split('__sep__').map((part, index) => (
                        <li key={index}>
                          <HighlightTypography
                            multiLevelKeywords={[
                              (colKeywords['statements'] || []).concat(
                                colKeywords['displayName'] || []
                              ),
                              row.keywords || [],
                            ]}
                            className={classes.statementText}
                          >
                            {part}
                          </HighlightTypography>
                        </li>
                      ))}
                    </ul>
                  </div>
                </Collapse>
              </TableCell>
            </TableRow>
          </React.Fragment>
        ))}
      </TableBody>
    );
  }
);

export default RealArticleTableBody;
