export class EntityOption {
  type = null;

  typeName = null;

  code = null;

  displayName = null;

  // pinyin name for stocks
  altName = null;

  label = null;

  highlightedOption = null;

  constructor(obj, type) {
    if (type === 'stock') {
      this.type = type;
      this.typeName = '股票';
      this.code = obj.symbol;
      this.displayName = obj.displayName;
      this.altName = obj.name;
      this.label = `#${obj.symbol}${obj.displayName}`;
      this.keyword = this.displayName;
    }
    if (type === 'block') {
      this.type = type;
      this.typeName = '板块';
      this.code = obj.code;
      this.displayName = obj.name;
      this.altName = obj.pinyinName;
      this.label = `${obj.name}`;
      this.keyword = this.displayName;
    }
    if (type === 'topic') {
      this.type = type;
      this.typeName = '关键字';
      this.code = obj.topicId;
      this.topic = obj.topic;
      this.displayName = obj.keyword;
      this.label = obj.keyword;
      this.keyword = this.displayName;
    }
    if (type === 'placeholder') {
      this.type = type;
      this.typeName = '';
      this.code = '';
      this.displayName = obj.name;
      this.label = obj.label || obj.name;
    }
  }
}

export const entityCompareFn = (a, b) => {
  if (a.type === b.type && a.type === 'topic') {
    if (a.topic === a.displayName && b.topic !== b.displayName) return -1;
    if (a.topic !== a.displayName && b.topic === b.displayName) return 1;
  }
  if (a.code < b.code) return -1;
  if (a.code > b.code) return 1;
  return 0;
};

export const entityMatchFn = (obj, keyword) => {
  // exact match
  if (
    keyword === obj.code ||
    keyword === obj.displayName ||
    keyword.toUpperCase() === obj.altName ||
    keyword === obj.label
  ) {
    return 2;
  }
  // fuzzy match
  //   code, startsWith or endsWith
  if (obj.code.startsWith(keyword) || obj.code.endsWith(keyword)) return 1;
  //   altName, startsWith
  if (obj.altName != null && obj.altName.startsWith(keyword.toUpperCase())) return 1;
  //   displayName, includes
  if (obj.displayName.includes(keyword)) return 1;
  // no match
  return 0;
};

export default EntityOption;
