import { action, observable, computed, runInAction } from 'mobx';
import { format, isValid, addDays } from 'date-fns';

import EntityOption, { entityCompareFn, entityMatchFn } from './EntityOption';
import {
  fetchStockAndBlockListData,
  getStockAndBlockListCache,
  setStockAndBlockListCache,
} from '../../../utils/stockAndBlockListCache';

export class Field {
  name = '';

  type = null;

  store = null;

  @observable value = null;

  @observable prevValue = null;

  @observable error = null;

  // keywords generated from value, for content highlighting
  @computed get keywords() {
    return [this.value];
  }

  @computed get changed() {
    return this.value !== this.prevValue;
  }

  @action.bound
  storePrevState() {
    this.prevValue = this.value;
  }

  @action.bound
  setValue(newValue) {
    this.value = newValue;
  }

  @action.bound
  setInitValue(value) {
    this.prevValue = value;
    this.value = value;
  }

  @action.bound
  validate() {
    throw 'Error: validate function not implemented.';
  }

  // parse field value to request parameter
  @action.bound
  parse() {
    throw 'Error: parse function not implemented.';
  }

  constructor(store, name) {
    this.store = store;
    this.name = name;
  }
}

export class GenericField extends Field {
  @action.bound
  validate() {
    return null;
  }

  @action.bound
  parse() {
    return this.value;
  }

  constructor(store, name) {
    super(store, name);
  }
}

export class TextField extends GenericField {}

export class BinaryField extends GenericField {}

export class DateTimeField extends Field {
  @action.bound
  validate() {
    if (!isValid(this.value)) {
      this.error = '日期不合法';
      return this.error;
    }
    return null;
  }

  @action.bound
  parse() {
    if (this.value != null) {
      return format(this.value, 'yyyy-MM-dd');
    }
    return null;
  }

  constructor(store, name) {
    super(store, name);
  }
}

export class EndDateField extends DateTimeField {
  @action.bound
  parse() {
    if (this.value != null) {
      return format(addDays(this.value, 1), 'yyyy-MM-dd');
    }
    return null;
  }
}

export class AutoCompleteTextField extends Field {
  @observable loading = false;

  @observable inputValue = '';

  @observable value = null;

  @observable prevValue = null;

  @observable options = [];

  @computed get visibleOptions() {
    throw 'Error: visibleOptions function not implemented.';
  }

  @action.bound
  validate() {
    return null;
  }

  @action.bound
  setLoading(value) {
    this.loading = value;
  }

  @action.bound
  setOptions(newOptions) {
    this.options = newOptions;
  }

  // one-time fetch on initialization
  @action.bound
  prefetchOptios() {
    throw 'Error: prefetchOptions function not implemented.';
  }

  @action.bound
  setInputValue(value) {
    this.inputValue = value;
  }

  // throttled fetch handler for real time suggestion
  @action.bound
  refreshOptions() {
    throw 'Error: refreshOptions function not implemented.';
  }

  constructor(store, name, props) {
    super(store, name);
  }
}

export class EntityAutoCompleteField extends AutoCompleteTextField {
  @computed get keywords() {
    if (this.value == null) {
      return [];
    }
    return [this.value.keyword];
  }

  @computed get visibleOptions() {
    if (this.loading) {
      return [];
    }
    const exactMatch = this.options.filter((obj) => entityMatchFn(obj, this.inputValue) == 2);
    const fuzzyMatch = this.options.filter((obj) => entityMatchFn(obj, this.inputValue) == 1);
    let result = [...exactMatch, ...fuzzyMatch];
    if (result.length > 6) {
      const totalLength = result.length;
      result = result.slice(0, 5);
      result.push(
        new EntityOption(
          {
            name: `...更多(${totalLength - 5})`,
            label: this.inputValue,
          },
          'placeholder'
        )
      );
    }
    return result;
  }

  parse() {
    if (this.value != null) {
      return this.value.code;
    }
    return null;
  }

  @action.bound
  parseOption(obj) {
    return new EntityOption(obj, this.type);
  }

  @action.bound
  async prefetchOptions() {
    this.setLoading(true);
    const data = await this.api.request();
    this.setOptions(data.map(this.parseOption).sort(entityCompareFn));
    this.setLoading(false);
  }

  constructor(store, name, props) {
    super(store, name);
    const { api, type } = props;
    this.api = api;
    this.type = type;
    this.setOptions([]);
  }
}

export class StockAutoCompleteField extends EntityAutoCompleteField {
  @action.bound
  async prefetchOptions() {
    this.setLoading(true);
    let cachedData = getStockAndBlockListCache();
    if (cachedData == null) {
      const [stockData, blockData] = await fetchStockAndBlockListData();
      setStockAndBlockListCache([stockData, blockData]);
      cachedData = getStockAndBlockListCache();
    }
    const data = cachedData.stockData;
    this.setOptions(data.map(this.parseOption).sort(entityCompareFn));
    this.setLoading(false);
  }
}

export class BlockAutoCompleteField extends EntityAutoCompleteField {
  @action.bound
  async prefetchOptions() {
    this.setLoading(true);
    let cachedData = getStockAndBlockListCache();
    if (cachedData == null) {
      const [stockData, blockData] = await fetchStockAndBlockListData();
      setStockAndBlockListCache([stockData, blockData]);
      cachedData = getStockAndBlockListCache();
    }
    const data = cachedData.blockData;
    this.setOptions(data.map(this.parseOption).sort(entityCompareFn));
    this.setLoading(false);
  }
}

export class TopicAutoCompleteField extends EntityAutoCompleteField {
  @computed get keywords() {
    if (this.value == null) {
      return [];
    }
    const result = this.options
      .filter((obj) => obj.code === this.value.code)
      .map((obj) => obj.keyword);
    return result;
  }
}

export default Field;
