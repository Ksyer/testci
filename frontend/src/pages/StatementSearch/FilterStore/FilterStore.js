import React from 'react';
import { action, observable, runInAction, asMap, computed } from 'mobx';
import { format, addDays, addMonths, isValid } from 'date-fns';
import uuid from 'node-uuid';

import {
  Field,
  TextField,
  BinaryField,
  DateTimeField,
  EndDateField,
  AutoCompleteField,
  EntityAutoCompleteField,
  StockAutoCompleteField,
  BlockAutoCompleteField,
  TopicAutoCompleteField,
} from './Field';

export class FilterStoreBase {
  @observable fieldList = [];

  @observable fieldState = {};

  @observable changed = false;

  @action.bound
  storePrevState() {
    this.fieldList.forEach((key) => {
      this.fieldState[key].storePrevState();
    });
  }

  @action.bound
  handleSubmit() {
    throw 'Error: method handleSubmit not defined.';
  }

  @action.bound
  handleValidate() {
    throw 'Error: method handleValidate not defined.';
  }

  @action.bound
  setValue(key, value, validate = true) {
    this.fieldState[key].setValue(value);
    if (validate) {
      this.handleValidate();
    }
    this.changed = this.fieldList.reduce((agg, key) => agg || this.fieldState[key].changed, false);
  }

  @computed get getValue() {
    return (key) => this.fieldState[key].value;
  }

  @computed get getError() {
    return (key) => this.fieldState[key].error;
  }
}

class FilterStore extends FilterStoreBase {
  // parse state into request params
  @action.bound
  parseParams() {
    const params = {};
    const paramList = [
      'fromDate',
      'toDate',
      'authorLike',
      'titleLike',
      'statementsLike',
      'relatedStock',
      'relatedBlock',
      'relatedTopic',
    ];
    const paramNameMap = {
      relatedStock: 'stockId',
      relatedBlock: 'blockId',
      relatedTopic: 'topicId',
    };
    paramList.forEach((key) => {
      if (this.fieldState[key].value != null) {
        const paramKey = paramNameMap[key] || key;
        params[paramKey] = this.fieldState[key].parse();
      }
    });
    const sourceList = [];
    if (this.fieldState.sourceWechat.value) {
      sourceList.push('wechat');
    }
    if (this.fieldState.sourceXueqiu.value) {
      sourceList.push('xueqiu');
    }
    if (this.fieldState.sourceReport.value) {
      sourceList.push('report');
    }
    params.articleSourceList = sourceList.join(',');
    return params;
  }
  // handleSubmit make query from fieldState
  // only proceed with submit if there is no error.
  @action.bound
  handleSubmit() {
    const errorFlag = this.handleValidate();
    if (!errorFlag) {
      this.storePrevState();
      this.changed = false;
      return this.store.getArticles(this.parseParams());
    }
  }

  // handleValidate validate fieldState and update fieldError
  @action.bound
  handleValidate() {
    let flag = false;
    // validate each field
    this.fieldList.forEach((key) => {
      const stateError = this.fieldState[key].validate();
      if (stateError != null) {
        flag = true;
      }
    });
    // cross field validation
    if (
      this.fieldState.fromDate.error == null &&
      this.fieldState.toDate.error == null &&
      this.fieldState.fromDate.value > this.fieldState.toDate.value
    ) {
      this.fieldState.fromDate.error = '开始日期须在截止日期之前';
      this.fieldState.toDate.error = '开始日期须在截止日期之前';
      flag = true;
    }
    return flag;
  }

  constructor(store) {
    super();
    this.store = store;
    this.fieldState.fromDate = new DateTimeField(this, 'fromDate');
    this.fieldState.fromDate.setInitValue(addMonths(new Date(), -6));
    this.fieldState.toDate = new EndDateField(this, 'toDate');
    this.fieldState.toDate.setInitValue(new Date());
    // article conditions
    this.fieldState.authorLike = new TextField(this, 'authorLike');
    this.fieldState.authorLike.setInitValue('');
    this.fieldState.titleLike = new TextField(this, 'titleLike');
    this.fieldState.titleLike.setInitValue('');
    this.fieldState.statementsLike = new TextField(this, 'statementsLike');
    this.fieldState.statementsLike.setInitValue('');
    // related entities
    this.fieldState.relatedStock = new StockAutoCompleteField(this, 'relatedStock', {
      api: this.store.apis.stockInfo,
      type: 'stock',
    });
    this.fieldState.relatedBlock = new BlockAutoCompleteField(this, 'relatedBlock', {
      api: this.store.apis.blockInfo,
      type: 'block',
    });
    this.fieldState.relatedTopic = new TopicAutoCompleteField(this, 'relatedTopic', {
      api: this.store.apis.topicKeyword,
      type: 'topic',
    });
    // sources
    this.fieldState.sourceWechat = new BinaryField(this, 'sourceWechat');
    this.fieldState.sourceWechat.setInitValue(true);
    this.fieldState.sourceXueqiu = new BinaryField(this, 'sourceXueqiu');
    this.fieldState.sourceXueqiu.setInitValue(true);
    this.fieldState.sourceReport = new BinaryField(this, 'sourceReport');
    this.fieldState.sourceReport.setInitValue(true);
    // initiate fieldList
    this.fieldList = Object.keys(this.fieldState);
  }
}

export default FilterStore;
