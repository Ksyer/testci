import React, { useEffect, useCallback, useRef } from 'react';
import { observer } from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  highlight: {
    fontWeight: 500,
    color: theme.palette.primary.main,
  },
  disabled: {
    color: 'rgba(0, 0, 0, 0.26)',
    cursor: 'default',
  },
}));

const DecorateText = ({ content, keyword }) => {
  const classes = useStyles();
  let head = '';
  let body = '';
  let tail = '';
  if (content.includes(keyword)) {
    const split0 = content.indexOf(keyword);
    const split1 = split0 + keyword.length;
    head = content.slice(0, split0);
    body = content.slice(split0, split1);
    tail = content.slice(split1);
  } else {
    head = content;
  }
  return (
    <Typography>
      <span>{head}</span>
      <span className={classes.highlight}>{body}</span>
      <span>{tail}</span>
    </Typography>
  );
};

const OptionSlug = observer(({ option, keyword }) => {
  const classes = useStyles();
  if (option.type === 'placeholder') {
    return (
      <Grid container justify="flex-end" className={classes.disabled}>
        <Typography>{option.displayName}</Typography>
      </Grid>
    );
  }
  return (
    <Grid container justify="space-between">
      <Grid item xs={8}>
        <Grid container justify="flex-start">
          <Typography color="primary" style={{ fontWeight: 500 }}>
            {option.typeName}
          </Typography>
          <div style={{ width: '0.5rem' }} />
          <DecorateText content={option.displayName} keyword={keyword} />
        </Grid>
      </Grid>
      <Grid item xs={4}>
        <Grid container justify="flex-end">
          <Grid item>
            <DecorateText content={`(${option.code})`} keyword={keyword} />
          </Grid>
          {option.extensionName && (
            <>
              <Grid item>
                <div style={{ width: '0.5rem' }} />
              </Grid>
              <Grid item>
                <Typography color="textSecondary">{option.extensionName}</Typography>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
});

const AutoCompleteField = observer(({ displayName, store, inputRef, ...props }) => {
  const {
    name,
    loading,
    inputValue,
    setInputValue,
    value,
    visibleOptions,
    prefetchOptions,
  } = store;
  const handleChangeValue = (value) => {
    // setValue through filterStore to properly handle changed state
    store.store.setValue(name, value);
  };
  useEffect(() => {
    // prefetch options on mount
    prefetchOptions();
  }, []);
  return (
    <Autocomplete
      autoHighlight
      options={visibleOptions}
      getOptionLabel={(option) => option.label}
      filterOptions={(x) => x}
      inputValue={inputValue}
      onInputChange={(e, newInputValue) => {
        setInputValue(newInputValue);
      }}
      value={value}
      onChange={(e, newValue) => {
        handleChangeValue(newValue);
      }}
      loading={loading}
      renderOption={(option) => <OptionSlug option={option} keyword={inputValue} />}
      renderInput={(params) => (
        <TextField
          {...params}
          id={name}
          label={displayName}
          placeholder={displayName}
          variant="outlined"
          fullWidth
          inputRef={inputRef}
        />
      )}
      {...props}
    />
  );
});

export default AutoCompleteField;
