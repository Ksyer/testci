import React from 'react';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { observer } from 'mobx-react';

const CheckboxWithLabel = observer(({ name, displayName, getValue, setValue }) => {
  const handleChange = (e) => {
    setValue(e.target.name, e.target.checked);
  };
  return (
    <FormControlLabel
      control={<Checkbox name={name} checked={getValue(name)} onChange={handleChange} />}
      label={displayName}
    />
  );
});

export default CheckboxWithLabel;
