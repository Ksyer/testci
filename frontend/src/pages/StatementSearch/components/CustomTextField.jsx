import React from 'react';
import { TextField } from '@material-ui/core';
import { observer } from 'mobx-react';

const CustomTextField = observer(({ name, displayName, getValue, setValue }) => {
  const handleChange = (e) => {
    setValue(name, e.target.value);
  };
  return (
    <TextField
      id={name}
      placeholder={displayName}
      label={displayName}
      value={getValue(name)}
      onChange={handleChange}
      variant="outlined"
      fullWidth
    />
  );
});

export default CustomTextField;
