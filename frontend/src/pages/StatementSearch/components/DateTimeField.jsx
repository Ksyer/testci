import React from 'react';
import { observer } from 'mobx-react';
import { KeyboardDatePicker } from '@material-ui/pickers';

const DateTimeField = observer(({ name, displayName, getValue, getError, setValue }) => {
  const handleChange = (value) => {
    setValue(name, value);
  };
  return (
    <KeyboardDatePicker
      disableFuture
      label={displayName}
      variant="inline"
      format="yyyy-MM-dd"
      id="date-picker-from"
      value={getValue(name)}
      error={getError(name) != null}
      helperText={getError(name)}
      onChange={handleChange}
    />
  );
});

export default DateTimeField;
