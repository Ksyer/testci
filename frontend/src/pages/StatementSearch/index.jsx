import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Button, Grid, Typography, FormGroup } from '@material-ui/core';
import { observer } from 'mobx-react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import AutoCompleteField from './components/AutoCompleteField';
import CheckboxWithLabel from './components/CheckboxWithLabel';
import DateTimeField from './components/DateTimeField';
import CustomTextField from './components/CustomTextField';
import { StoreProvider, useStore } from './store';
import useStyles from '../ArticleList/useStyles';
import Layout from '../../components/Layout/Layout';
import ArticleTable from '../ArticleList/views/ArticleTable';
import { statementListConfig } from './config';

const appName = '段落搜索';

const StatementSearchView = observer(() => {
  const store = useStore();
  const classes = useStyles();
  const {
    getArticles,
    handleFormSubmit,
    filterStore: { fieldState, changed, setValue, getValue, getError },
  } = store;
  useEffect(() => {
    getArticles({
      page: 1,
    });
  }, []);
  return (
    <Layout>
      <Helmet>
        <title>{appName}</title>
      </Helmet>
      <Grid container spacing={3} className={classes.wrap}>
        <Grid item xs={12}>
          <Typography variant="h5">{appName}</Typography>
        </Grid>
        <Grid item xs={12}>
          <form onSubmit={handleFormSubmit}>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Typography color="primary">文章信息</Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <CustomTextField
                      name="titleLike"
                      displayName="文章标题"
                      getValue={getValue}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <CustomTextField
                      name="authorLike"
                      displayName="文章作者"
                      getValue={getValue}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <CustomTextField
                      name="statementsLike"
                      displayName="段落内容"
                      getValue={getValue}
                      setValue={setValue}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Typography color="primary">关联属性</Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <AutoCompleteField displayName="相关个股" store={fieldState.relatedStock} />
                  </Grid>
                  <Grid item xs={4}>
                    <AutoCompleteField displayName="相关板块" store={fieldState.relatedBlock} />
                  </Grid>
                  <Grid item xs={4}>
                    <AutoCompleteField displayName="相关话题" store={fieldState.relatedTopic} />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Typography color="primary">文章来源</Typography>
              </Grid>
              <Grid item xs={4}>
                <FormGroup row>
                  <CheckboxWithLabel
                    name="sourceReport"
                    displayName="研报"
                    getValue={getValue}
                    setValue={setValue}
                  />
                  <CheckboxWithLabel
                    name="sourceWechat"
                    displayName="微信"
                    getValue={getValue}
                    setValue={setValue}
                  />
                  <CheckboxWithLabel
                    name="sourceXueqiu"
                    displayName="雪球"
                    getValue={getValue}
                    setValue={setValue}
                  />
                </FormGroup>
              </Grid>
              <Grid item xs={4} style={{ padding: '0 8px' }}>
                <Grid container justify="flex-end" alignContent="center" spacing={2}>
                  <Grid item xs={6}>
                    <DateTimeField
                      displayName="开始日期"
                      name="fromDate"
                      getValue={getValue}
                      getError={getError}
                      setValue={setValue}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <DateTimeField
                      displayName="截止日期"
                      name="toDate"
                      getValue={getValue}
                      getError={getError}
                      setValue={setValue}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                container
                item
                xs={4}
                justify="flex-end"
                alignContent="center"
                style={{ padding: '0 8px' }}
              >
                <Typography color="error" className={classes.centerText}>
                  {changed ? '条件已改变，请重新提交表单' : ''}
                </Typography>
                <div style={{ width: '1rem' }} />
                <Grid item style={{ alignSelf: 'center' }}>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.searchButton}
                    type="submit"
                  >
                    筛选
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={12}>
          <ArticleTable title="段落列表" store={store} config={statementListConfig} />
        </Grid>
      </Grid>
    </Layout>
  );
});

const StatementSearchPage = observer(() => {
  return (
    <StoreProvider>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <StatementSearchView />
      </MuiPickersUtilsProvider>
    </StoreProvider>
  );
});

export default StatementSearchPage;
