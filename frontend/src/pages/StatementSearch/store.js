import React from 'react';
import { action, computed, observable, runInAction } from 'mobx';
import { format, addDays, addMonths, isValid } from 'date-fns';
import uuid from 'node-uuid';

import FilterStore from './FilterStore/FilterStore';
import { statementSearchV2ApiInstance } from '../../apis/StatementSearchV2Api';
import { stockInfoApiInstance } from '../../apis/StockInfoApi';
import { blockInfoApiInstance } from '../../apis/BlockInfoApi';
import { topicKeywordApiInstance } from '../../apis/TopicKeywordApi';

class Store {
  @observable pageSize = 15;

  @observable articleList = null;

  @observable loading = false;

  @observable page = 1;

  @observable totalCount = null;

  @observable focusedRow = -1;

  signature = null;

  @computed get colKeywords() {
    const result = {};
    if (this.filterStore == null || this.filterStore.fieldState == null) {
      return result;
    }
    result.related_entity_list = [
      ...this.filterStore.fieldState.relatedStock.keywords,
      ...this.filterStore.fieldState.relatedBlock.keywords,
    ];
    result.related_topic_list = this.filterStore.fieldState.relatedTopic.keywords;
    result.article_author = this.filterStore.fieldState.authorLike.keywords;
    result.article_title = this.filterStore.fieldState.titleLike.keywords;
    result.statements = [
      ...this.filterStore.fieldState.statementsLike.keywords,
      ...this.filterStore.fieldState.relatedTopic.keywords,
    ];
    return result;
  }

  @action.bound
  getArticles(params) {
    const defaultParams = {
      page: 1,
      pageSize: this.pageSize,
    };
    this.focusedRow = -1;
    const requestParams = {
      ...defaultParams,
      ...this.filterStore.parseParams(),
      ...params,
    };
    this.loading = true;
    const signature = uuid.v4();
    this.signature = signature;
    return this.apis.statementSearch
      .request(requestParams)
      .then((data) => {
        if (this.signature === signature) {
          runInAction(() => {
            this.articleList = data.results.map((obj) => ({
              key: obj.statement_id,
              author_updatetime: `${obj.article_author}\n${obj.article_updatetime}`,
              related_entity_list: obj.related_stock_list
                .map((obj) => [
                  `#${obj.a_code}${obj.display_name}`,
                  `/stock-app/stocks/${obj.a_code}/`,
                ])
                .concat(
                  obj.related_block_list.map((obj) => [
                    `#${obj.block_code}${obj.block_name}`,
                    `/stock-app/blocks/${obj.block_code}`,
                  ])
                ),
              related_topic_list:
                obj.related_topic_list.map((obj) => `${obj.topic}`).join(' ') || '(无)',
              article_title: [`${obj.article_source} - ${obj.article_title}`, obj.article_url],
              statements: obj.statements,
              keywords: obj.related_stock_list
                .map((obj) => obj.display_name)
                .concat(obj.related_block_list.map((obj) => obj.block_name)),
            }));
            this.totalCount = data.count;
            this.page = requestParams.page;
            this.loading = false;
          });
        }
      })
      .catch((err) => {
        console.error(err);
        runInAction(() => {
          this.articleList = [];
          this.loading = false;
        });
      });
  }

  @action.bound
  setPage({ page }) {
    if (this.page == page) {
      return;
    }
    this.getArticles({ page });
  }

  @action.bound
  handleChangePage(e, page) {
    this.setPage({ page: page + 1 });
  }

  @action.bound
  handleChangeFromDate(date) {
    this.filterStore.setValue('fromDate', date);
  }

  @action.bound
  handleChangeToDate(date) {
    this.filterStore.setValue('toDate', date);
  }

  @action.bound
  handleChangeField(e) {
    const key = e.target.id;
    this.filterStore.setValue(key, e.target.value);
  }

  @action.bound
  handleFormSubmit(e) {
    e.preventDefault();
    this.filterStore.handleSubmit();
  }

  // for expanding statements details
  @action.bound
  toggleFocusedRow(rowIdx) {
    if (this.focusedRow === rowIdx) {
      this.focusedRow = -1;
      return;
    }
    this.focusedRow = rowIdx;
  }

  dispose() {
    this.articleList = [];
    this.page = 1;
    this.totalCount = null;
    this.fromDate = null;
    this.toDate = null;
  }

  constructor() {
    this.apis = {
      statementSearch: statementSearchV2ApiInstance,
      stockInfo: stockInfoApiInstance,
      blockInfo: blockInfoApiInstance,
      topicKeyword: topicKeywordApiInstance,
    };
    this.filterStore = new FilterStore(this);
  }
}

const storeInstance = new Store();

const storeContext = React.createContext(null);

export const StoreProvider = ({ children }) => (
  <storeContext.Provider value={storeInstance}>{children}</storeContext.Provider>
);

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) throw new Error('useStore must be used within a StoreProvider.');
  return store;
};

export default Store;
