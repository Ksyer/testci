import React from 'react';
import { observer } from 'mobx-react';
import { Helmet } from 'react-helmet-async';
import { Switch, Redirect, Route, useRouteMatch, useParams } from 'react-router-dom';

import { StoreProvider } from './stores/store';
import View from './views/View';

const StockApp = observer(() => {
  const { path, url } = useRouteMatch();
  const { entityType } = useParams();
  return (
    <>
      <Helmet>
        <title>股票分析</title>
      </Helmet>
      <StoreProvider>
        <Switch>
          <Route
            path={`${path}`}
            exact
            render={() => {
              if (entityType === 'stocks') {
                return <Redirect to={url.replace(/([^/]+)\/?$/, '$1/info')} />;
              }
              if (entityType === 'blocks') {
                return <Redirect to={url.replace(/([^/]+)\/?$/, '$1/compare')} />;
              }
            }}
          />
          <Route path={`${path}/:appId`} component={View} />
        </Switch>
      </StoreProvider>
    </>
  );
});

export default StockApp;
