import { action, observable, runInAction } from 'mobx';

import drawGraph from '../../../../utils/cytoscapeGraph/drawGraph';

class BlockGraphStore {
  store = null;

  blockCode = null;

  graphData = null;

  graphElement = null;

  clickHandler = null;

  cytoscapeInstance = null;

  @observable graphInfo = '';

  @action.bound
  setGraphElement(element) {
    this.graphElement = element;
  }

  @action.bound
  async fetchData() {
    const { entityType, currentBlock, focusedBlockCode } = this.store;
    const api = this.store.apis.graphCrawler;
    if (entityType === 'blocks') {
      if (currentBlock == null) {
        return;
      }
      this.blockCode = currentBlock.code;
    } else if (entityType === 'stocks') {
      if (focusedBlockCode == null) {
        return;
      }
      this.blockCode = focusedBlockCode;
    } else {
      throw `Error: Entity Type ${entityType} not recognized.`;
    }
    const { data } = await api.request({ nodeId: this.blockCode, queryType: 'trunk' });
    this.graphData = data;
    return data;
  }

  @action.bound
  setGraphInfo(message) {
    this.graphInfo = message;
  }

  @action.bound
  async drawGraph(preDrawFn) {
    this.setGraphInfo('正在获取行业关联信息...');
    const graphData = await this.fetchData();
    // abort if graph is empty
    if (graphData == null || !Array.isArray(graphData.nodes) || graphData.nodes.length <= 0) {
      this.setGraphInfo('无行业关联信息');
      if (this.cytoscapeInstance != null) {
        this.cytoscapeInstance.unmount();
        this.cytoscapeInstance.destroy();
      }
      return;
    }
    this.setGraphInfo('');
    const { entityType, graphBlockCode } = this.store;
    let handlerFunc = null;
    if (entityType === 'blocks') {
      const { navigate } = this.store;
      handlerFunc = (blockCode) => {
        navigate(blockCode, 'blocks', 'compare');
      };
    }
    if (entityType === 'stocks') {
      const { setFocusedBlock, relatedBlocks } = this.store;
      const relatedBlocksCodeList = relatedBlocks.map((obj) => obj.code);
      graphData.nodes.forEach((obj) => {
        obj.data.disabled = relatedBlocksCodeList.includes(obj.data.id) ? 'false' : 'true';
      });
      handlerFunc = (blockCode) => {
        setFocusedBlock(blockCode);
      };
    }
    const graphOptions = {
      subjectNodeId: graphBlockCode,
      container: this.graphElement,
      vertical: true,
      onClick: (cyNode) => {
        const { id: blockCode } = cyNode.data();
        handlerFunc(blockCode);
      },
    };
    if (this.cytoscapeInstance != null) {
      this.cytoscapeInstance.unmount();
      this.cytoscapeInstance.destroy();
    }
    preDrawFn();
    this.cytoscapeInstance = drawGraph(graphData, graphOptions);
  }

  @action.bound
  handleResize() {
    if (this.cytoscapeInstance != null) {
      this.cytoscapeInstance.resize();
      this.cytoscapeInstance.fit(null, 20);
    }
  }

  @action.bound
  disposeGraph() {
    if (this.cytoscapeInstance == null) return;
    this.cytoscapeInstance.unmount();
    this.cytoscapeInstance.destroy();
  }

  @action.bound
  dispose() {
    this.setGraphInfo('');
  }

  constructor(store) {
    this.store = store;
  }
}

export default BlockGraphStore;
