import { observable, action, computed } from 'mobx';
import { TopicAutoCompleteField } from '../../../StatementSearch/FilterStore/Field';

import { FilterStoreBase } from '../../../StatementSearch/FilterStore/FilterStore';
import StockBlockField from './StockBlockField';

const parseEntityType = (type) => {
  switch (type) {
    case 'stock':
      return 'stocks';
    case 'block':
      return 'blocks';
    default:
      throw `Error: Unrecognized entity type ${type}`;
  }
};

class EntitySearchStore extends FilterStoreBase {
  @computed get entityIsBlock() {
    if (this.fieldState.entity.value != null && this.fieldState.entity.value.type === 'block') {
      return true;
    }
    return false;
  }

  @computed get loading() {
    // WIP this.fieldState.entity.loading || this.fieldState.extension.loading
    return this.fieldState.entity.loading;
  }

  @action.bound
  handleSubmit() {
    if (this.inputValue === '') return;
    if (this.loading) return;
    const targetEntity = this.fieldState.entity.value;
    const targetTopic = this.fieldState.topic.value;
    if (targetEntity != null && (targetEntity.type === 'stock' || targetEntity.type === 'block')) {
      if (targetTopic != null) {
        this.store.navigate(
          targetEntity.code,
          parseEntityType(targetEntity.type),
          'recommendation'
        );
        this.store.statementSearchStore.filterStore.setValue('relatedTopic', targetTopic);
      } else {
        this.store.navigate(targetEntity.code, parseEntityType(targetEntity.type));
      }
    }
  }

  @action.bound
  handleValidate() {
    return false;
  }

  constructor(store) {
    super();
    this.store = store;
    this.fieldState.entity = new StockBlockField(this, 'entity', {
      stockApi: this.store.apis.stockInfoApi,
      blockApi: this.store.apis.blockInfoApi,
    });
    this.fieldState.entity.prefetchOptions();
    this.fieldState.topic = new TopicAutoCompleteField(this, 'topic', {
      api: this.store.apis.topicKeyword,
      type: 'topic',
    });
    this.fieldState.topic.prefetchOptions();
    this.fieldList = Object.keys(this.fieldState);
  }
}

export default EntitySearchStore;
