import { action, observable, computed, runInAction } from 'mobx';

import { AutoCompleteTextField } from '../../../StatementSearch/FilterStore/Field';
import EntityOption, {
  entityCompareFn,
  entityMatchFn,
} from '../../../StatementSearch/FilterStore/EntityOption';
import {
  getStockAndBlockListCache,
  setStockAndBlockListCache,
  fetchStockAndBlockListData,
} from '../../../../utils/stockAndBlockListCache';

class StockBlockField extends AutoCompleteTextField {
  @computed get keywords() {
    if (this.value == null) {
      return [];
    }
    return [this.value.keyword];
  }

  @computed get visibleOptions() {
    if (this.loading) {
      return [];
    }
    const exactMatch = this.options.filter((obj) => entityMatchFn(obj, this.inputValue) == 2);
    const fuzzyMatch = this.options.filter((obj) => entityMatchFn(obj, this.inputValue) == 1);
    let stockResult = [
      ...exactMatch.filter((obj) => obj.type === 'stock'),
      ...fuzzyMatch.filter((obj) => obj.type === 'stock'),
    ];
    if (stockResult.length > 5) {
      stockResult = stockResult.slice(0, 4);
      stockResult.push(
        new EntityOption(
          {
            name: `...全部相关股票 (${stockResult.length})`,
            label: this.inputValue,
          },
          'placeholder'
        )
      );
    }
    let blockResult = [
      ...exactMatch.filter((obj) => obj.type === 'block'),
      ...fuzzyMatch.filter((obj) => obj.type === 'block'),
    ];
    if (blockResult.length > 5) {
      blockResult = blockResult.slice(0, 4);
      blockResult.push(
        new EntityOption(
          {
            name: `...全部相关板块 (${blockResult.length})`,
            label: this.inputValue,
          },
          'placeholder'
        )
      );
    }
    const placeholderResult = [];
    if (this.inputValue.length > 0) {
      placeholderResult.push(
        new EntityOption(
          { name: `...更多有关 "${this.inputValue}"`, label: this.inputValue },
          'placeholder'
        )
      );
    }
    return [...stockResult, ...blockResult, ...placeholderResult];
  }

  parse() {
    if (this.value != null) {
      return { code: this.value.code, type: this.value.type };
    }
  }

  @action.bound
  async prefetchOptions() {
    this.setLoading(true);
    // attempt to load data from cache
    let cachedData = getStockAndBlockListCache();
    // if failed, fetch data from server
    if (cachedData == null) {
      const [stockData, blockData] = await fetchStockAndBlockListData();
      setStockAndBlockListCache([stockData, blockData]);
      cachedData = getStockAndBlockListCache();
    }
    const stockData = cachedData.stockData;
    const blockData = cachedData.blockData;
    const options = [
      ...stockData.map((obj) => new EntityOption(obj, 'stock')).sort(entityCompareFn),
      ...blockData.map((obj) => new EntityOption(obj, 'block')).sort(entityCompareFn),
    ];
    this.setOptions(options);
    this.setLoading(false);
  }

  constructor(store, name, props) {
    super(store, name);
    const { stockApi, blockApi } = props;
    this.stockApi = stockApi;
    this.blockApi = blockApi;
    this.setOptions([]);
  }
}

export default StockBlockField;
