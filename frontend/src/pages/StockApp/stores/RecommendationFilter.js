import { action, observable } from 'mobx';
import { wechatRecommendationConfig } from '../../../consts/displayConfig';
import throttle from 'lodash/throttle';
import { Parser } from 'expr-eval';

const subStringFilter = (param, content) => Boolean(!param || String(content).indexOf(param) >= 0);

const comparisonFilter = (param, content) => {
  const parser = new Parser();
  let result = true;
  try {
    result = parser.evaluate(`x ${param}`, { x: content });
  } catch (err) {
    result = true;
  }
  return result;
};

export const filterContent = (param, content) => {
  if (
    String(param).startsWith('>') ||
    String(param).startsWith('<') ||
    String(param).startsWith('=')
  ) {
    return comparisonFilter(param, content);
  }
  return subStringFilter(param, content);
};

class RecommendationFilter {
  cols = [];

  @observable data = [];

  @observable filters = {};

  @action.bound
  applyFilters() {
    // if there is no valid filter, automaticall set active to true for all rows
    let defaultVal = true;
    for (let key of this.cols) {
      if (this.filters[key]) {
        defaultVal = false;
        break;
      }
    }
    this.data.forEach((obj) => {
      let targetVal = true;
      // defaultVal = false means there are active filters
      if (!defaultVal) {
        for (let key of this.cols) {
          targetVal = targetVal && filterContent(this.filters[key], obj[key]);
        }
      }
      if (obj.active !== targetVal) {
        obj.active = targetVal;
      }
    });
  }

  @action.bound
  fetchData() {
    this.data = this.store.currentStock.wechatRecommendations;
    this.data.forEach((obj) => {
      obj.active = true;
    });
  }

  @action.bound
  setFilter(key, value) {
    this.filters[key] = value;
    this.applyFiltersThrottled();
    // this.applyFilters();
  }

  @action.bound
  initiate() {
    this.cols.forEach((colKey) => {
      this.filters[colKey] = '';
    });
  }

  applyFiltersThrottled = throttle(this.applyFilters, 300);

  constructor(store) {
    this.store = store;
    this.cols = wechatRecommendationConfig.cols;
    this.initiate();
  }
}

export default RecommendationFilter;
