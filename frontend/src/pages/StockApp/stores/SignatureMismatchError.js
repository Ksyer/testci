class SignatureMismatchError extends Error {
  constructor(message) {
    super();
    this.name = 'SignatureMismatchError';
    this.message = message;
  }
}

export default SignatureMismatchError;
