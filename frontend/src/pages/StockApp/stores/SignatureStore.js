import uuid from 'node-uuid';

class SignatureStore {
  signatureDict = null;

  sign(key) {
    // console.log(`Signing signature for ${key}.`);
    this.signatureDict[key] = uuid.v4();
    return this.signatureDict[key];
  }

  verify(key, value) {
    // console.log(`Checking signature for ${key}.`);
    // console.log(`Expected: ${this.signatureDict[key]}; Found: ${value}.`);
    if (this.signatureDict[key] == null) return false;
    return this.signatureDict[key] === value;
  }

  constructor() {
    this.signatureDict = {};
  }
}

export default SignatureStore;
