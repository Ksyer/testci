interface StockAppStoreType {
  currentStock: StockModel;
  relatedIndustries: IndustryModel[];
  // industry_code
  activeIndustry: string;
  relatedStocks: StockModel[];
}

interface IndustryModelType {
  code: string;
  name: string;
}

interface StockModelType {
  code: string;
  symbol: string;
  display_name: string;
  name: string;
  financeData: FinanceData;
  businessData: BusinessData;
  recommendationData: RecommendationData;
  // preview stock model only has one row of financeData, no businessData or recommendationData
  preview: boolean;
}

// order by report_date desc
interface FinanceDataType {
  [paramName: string]: string;
}
[];

interface BusinessDataType {
  [date: string]: {
    [category: string]: {
      [businessLine: string]: string;
    };
  };
  dateList: string[];
}

// order by report_date desc
interface RecommendationData {
  [paramName: string]: string;
}
[];

// order by report_date desc
interface ValuationData {
  [paramName: string]: string;
}
[];
