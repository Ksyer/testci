import { action, computed, observable } from 'mobx';

class BlockModel {
  code = null;

  name = null;

  subType = null;

  category = null;

  @observable financeData = null;

  @observable peerFinanceData = null;

  @action.bound
  setFinanceData(data) {
    this.financeData = data;
  }

  constructor({ code, name, subType = null, category = null }) {
    this.code = code;
    this.name = name;
    this.subType = subType;
    this.category = category;
  }
}

export default BlockModel;
