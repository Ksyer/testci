class IndustryModel {
  code = null;
  name = null;
  constructor({ code, name }) {
    this.code = code;
    this.name = name;
  }
}

export default IndustryModel;
