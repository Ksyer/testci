import { action, computed, observable } from 'mobx';

class StockModel {
  code = null;

  symbol = null;

  // Chinese stock name
  displayName = null;

  // Letter acronym
  name = null;

  preview = false;

  @observable financeData = null;

  @observable peerFinanceData = null;

  @observable businessData = null;

  @observable wechatRecommendations = null;

  @observable valuationData = null;

  @observable newsEventData = null;

  @action.bound
  setFinanceData(data) {
    this.financeData = data;
  }

  constructor({ code, symbol, displayName, name }) {
    this.code = code;
    this.symbol = symbol;
    this.displayName = displayName;
    this.name = name;
  }
}

export default StockModel;
