import { filterContent } from './RecommendationFilter';

it('Filters contents by substring filter', () => {
  expect(filterContent('', 'abc')).toBe(true);
  expect(filterContent('a', 'abc')).toBe(true);
  expect(filterContent('a', 'bcd')).toBe(false);
});

it('Filters contents by comparison filter', () => {
  expect(filterContent('>2', 3)).toBe(true);
  expect(filterContent('<2', 3)).toBe(false);
  expect(filterContent('>=3', 3)).toBe(true);
  expect(filterContent('<=3', 3)).toBe(true);
  expect(filterContent('>=4', 3)).toBe(false);
  expect(filterContent('<=2', 3)).toBe(false);
  expect(filterContent(`>'2020-07-01'`, '2020-07-02')).toBe(true);
  expect(filterContent(`>'2020-07-03'`, '2020-07-02')).toBe(false);
});
