import React from 'react';
import { action, computed, observable, runInAction } from 'mobx';

import { history } from '../../../App';
import BlockGraphStore from './BlockGraph/BlockGraphStore';
import StockModel from './models/StockModel';
import BlockModel from './models/BlockModel';

import { blockInfoApiInstance } from '../../../apis/BlockInfoApi';
import { blockSummaryLatestApiInstance } from '../../../apis/BlockSummaryLatestApi';
import { stockInfoApiInstance } from '../../../apis/StockInfoApi';
import { financeRatioApiInstance } from '../../../apis/FinanceRatioApi';
import { relatedIndustriesApiInstance } from '../../../apis/RelatedIndustriesApi';
import { relatedStocksApiInstance } from '../../../apis/RelatedStocksApi';
import { businessAnalysisApiInstance } from '../../../apis/BusinessAnalysiApi';
import { wechatRecommendationApiInstance } from '../../../apis/WechatRecommendationApi';
import { statementSearchV2ApiInstance } from '../../../apis/StatementSearchV2Api';
import { valuationApiInstance } from '../../../apis/ValuationApi';
import { relatedBlocksApiInstance } from '../../../apis/RelatedBlocksApi';
import { blockSecuritiesApiInstance } from '../../../apis/BlockSecuritiesApi';
import { securityBlocksV2ApiInstance } from '../../../apis/SecurityBlocksV2';
import { blockSecuritiesV2ApiInstance } from '../../../apis/BlockSecuritiesV2';
import { graphCrawlerApiInstance } from '../../../apis/GraphCrawlerApi';
import { newsEventApiInstance } from '../../../apis/NewsEventApi';
import SignatureStore from './SignatureStore';
import SignatureMismatchError from './SignatureMismatchError';
import ArticleStore from '../../ArticleList/recommendationStore';
import StatementSearchStore from '../../StatementSearch/store';
import EntitySearchStore from './EntitySerach/EntitySearchStore';
import { topicKeywordApiInstance } from '../../../apis/TopicKeywordApi';

const tabCategory = ['report', 'year', 'quarter'];

class Store {
  @observable signatureStore = null;

  @observable articleStore = null;

  @observable entityType = null;

  @observable currentBlock = null;

  @observable currentStock = null;

  @observable relatedBlocks = null;

  @observable relatedStocks = null;

  // stock-info view

  @observable focusedRow = null;

  @observable focusedTab = 0;

  @observable displayedTab = 0;

  @observable tabLoading = false;

  @observable historyExpanded = false;

  // stock-compare view

  @observable blockLoading = false;

  @observable defaultBlockCode = null;

  @observable focusedBlockCode = null;

  @computed get focusedBlockName() {
    if (this.relatedBlocks == null) return '行业基线';
    if (this.focusedBlockCode == null) return '行业基线';
    for (let block of this.relatedBlocks) {
      if (block.code === this.focusedBlockCode) {
        return block.name;
      }
    }
    return '行业基线';
  }

  @computed get graphBlockCode() {
    if (this.entityType === 'stocks') {
      return this.focusedBlockCode;
    } else if (this.entityType === 'blocks') {
      if (this.currentBlock == null) return null;
      return this.currentBlock.code;
    }
    throw `Error: Invalide entityType ${this.entityType}`;
  }

  // wechat-recommendation search

  @observable quickSearchField = '';

  @observable quickSearchError = null;

  checkSignature(key, signature) {
    if (!this.signatureStore.verify(key, signature)) {
      throw new SignatureMismatchError(`Signature ${key} mismatch. Data discarded.`);
    }
  }

  makeLink(validatedCode, entityType = 'stocks', appId = null) {
    if (entityType === 'stocks') {
      let targetUrl = `/stock-app/stocks/${validatedCode}`;
      if (appId != null) {
        targetUrl += `/${appId}`;
      }
      return targetUrl;
    }
    if (entityType === 'blocks') {
      let targetUrl = `/stock-app/blocks/${validatedCode}`;
      if (appId != null) {
        targetUrl += `/${appId}`;
      }
      return targetUrl;
    }
    throw `Error: entityType ${entityType} not recognized.`;
  }

  @action.bound
  navigate(validatedCode, entityType = 'stocks', appId = null) {
    let targetUrl = this.makeLink(validatedCode, entityType, appId);
    let currentCode = null;
    if (this.entityType === 'stocks') {
      currentCode = this.currentStock.symbol;
    }
    if (this.entityType === 'blocks') {
      currentCode = this.currentBlock.code;
    }
    if (entityType !== this.entityType || validatedCode !== currentCode) {
      this.dispose('navigate');
    }
    history.push(targetUrl);
  }

  @action.bound
  quickNavigate(e) {
    e.preventDefault();
    const value = ('000000' + this.quickSearchField).slice(-6);
    if (Number.isInteger(Number(value)) && String(value).length === 6) {
      this.navigate(value);
      return;
    }
    this.quickSearchError = '请输入少于 6 位数字';
  }

  @action.bound
  getFinanceData() {
    let targetCode = null;
    let targetType = null;
    if (this.entityType === 'stocks') {
      if (this.currentStock == null) return;
      targetCode = this.currentStock.symbol;
      targetType = 'stocks';
    } else if (this.entityType === 'blocks') {
      if (this.currentBlock == null) return;
      targetCode = this.currentBlock.code;
      targetType = 'blocks';
    }
    const signatureKey = 'getFinanceData';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.financeRatioApi
      .request({
        // stockId: this.currentStock.symbol,
        stockId: targetCode,
        limit: 10,
        by: tabCategory[this.focusedTab],
      })
      .then((data) => {
        // discard data if signature does not match
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          if (targetType === 'stocks') {
            this.currentStock.setFinanceData(data);
          } else if (targetType === 'blocks') {
            this.currentBlock.setFinanceData(data);
          }
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        if (this.currentStock == null) return;
        runInAction(() => {
          if (targetType === 'stocks') {
            this.currentStock.setFinanceData([]);
          } else if (targetType === 'blocks') {
            this.currentBlock.setFinanceData([]);
          }
        });
      });
  }

  @action.bound
  getRelatedBlocks() {
    if (this.currentStock == null) return;
    const signatureKey = 'getRelatedBlocks';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.securityBlocksV2
      .request({ code: this.currentStock.code })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        const relatedBlocks = data.map(
          (block) =>
            new BlockModel({
              code: block.block_code,
              name: block.block_name,
              subType: block.sub_type,
              category: block.category,
            })
        );
        runInAction(() => {
          this.relatedBlocks = relatedBlocks;
          this.defaultBlockCode = relatedBlocks.sort((a, b) => {
            if (a.subType === b.subType) return 0;
            if (a.subType === 'industry') return -1;
            return 1;
          })[0].code;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        if (this.currentStock == null) return;
        runInAction(() => {
          this.relatedBlocks = [];
        });
      });
  }

  @action.bound
  getBusinessData() {
    if (this.currentStock == null) return;
    const signatureKey = 'getBusinessData';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.businessAnalysis
      .request({ stockId: this.currentStock.symbol })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentStock.businessData = data;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        if (this.currentStock == null) return;
        runInAction(() => {
          this.currentStock.businessData = null;
        });
      });
  }

  @action.bound
  getWechatRecommendations() {
    if (this.entityType === 'stocks') {
      if (this.currentStock == null) return;
      return this.articleStore.getArticles({
        page: 1,
        state: { stockId: this.currentStock.symbol },
      });
    }
    if (this.entityType === 'blocks') {
      if (this.currentBlock == null) return;
      this.statementSearchStore.filterStore.setValue('relatedBlock', {
        code: this.currentBlock.code,
        keyword: this.currentBlock.name,
      });
      return this.statementSearchStore.filterStore.handleSubmit();
    }
  }

  @action.bound
  getNewsEvent() {
    if (this.currentStock == null) return;
    const signatureKey = 'getNewsEvent';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.newsEvent
      .request({ stockId: this.currentStock.code })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentStock.newsEventData = data;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        if (this.currentStock == null) return;
        runInAction(() => {
          this.currentStock.newsEventData = [];
        });
      });
  }

  @action.bound
  getValuation() {
    if (this.currentStock == null) return;
    const signatureKey = 'getValuation';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.valuation
      .request({ stockId: this.currentStock.code })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentStock.valuationData = data;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        if (this.currentStock == null) return;
        runInAction(() => {
          this.currentStock.valuationData = [];
        });
      });
  }

  @action.bound
  getCurrentStock({ stockId }) {
    // get current stock model
    if (this.currentStock && this.currentStock.symbol == stockId) return;
    // clear out data
    this.dispose('get current stock');
    this.entityType = 'stocks';
    const signatureKey = 'getCurrentStock';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.stockInfoApi
      .request({ stockIds: [stockId] })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentStock = new StockModel(data[0]);
        });
      })
      .then(() => {
        Promise.all([
          this.getFinanceData(),
          this.getRelatedBlocks(),
          this.getBusinessData(),
          this.getWechatRecommendations(),
          this.getNewsEvent(),
        ]);
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        runInAction(() => {
          this.currentStock = new StockModel({
            code: stockId,
            symbol: stockId,
            displayName: '未找到股票',
            name: '',
          });
        });
      });
  }

  @action.bound
  getCurrentBlock({ blockId }) {
    if (this.currentBlock && this.currentBlock.symbol == blockId) return;
    this.dispose('get current block');
    this.entityType = 'blocks';
    const signatureKey = 'getCurrentBlock';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.blockInfoApi
      .request({ blockCode: blockId })
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentBlock = new BlockModel(data[0]);
        });
      })
      .then(() => {
        Promise.all([
          this.getFinanceData(),
          this.getRelatedStocks({ blockCode: blockId }),
          this.getBlockSummary({ blockCode: blockId }),
          this.getWechatRecommendations(),
        ]);
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        runInAction(() => {
          this.currentBlock = new BlockModel({
            code: blockId,
            name: '',
          });
        });
      });
  }

  @action.bound
  getRelatedStocks({ blockCode = null }) {
    const params = { blockCode };
    const signatureKey = 'getRelatedStocks';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.blockSecuritiesV2
      .request(params)
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        const relatedSecurities = data;
        runInAction(() => {
          const relatedStocksBuffer = [];
          relatedSecurities.forEach((obj) => {
            const newRelatedStock = new StockModel(obj.stockInfo);
            newRelatedStock.peerFinanceData = obj.peerFinanceData;
            relatedStocksBuffer.push(newRelatedStock);
            if (this.currentStock != null && newRelatedStock.code == this.currentStock.code)
              this.currentStock.peerFinanceData = obj.peerFinanceData;
          });
          this.relatedStocks = relatedStocksBuffer;
          this.blockLoading = false;
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        runInAction(() => {
          this.relatedStocks = [];
          this.blockLoading = false;
        });
      });
  }

  @action.bound
  getBlockSummary({ blockCode = null }) {
    const params = { blockCode };
    const signatureKey = 'getBlockSummary';
    const signature = this.signatureStore.sign(signatureKey);
    return this.apis.blockSummaryLatest
      .request(params)
      .then((data) => {
        this.checkSignature(signatureKey, signature);
        runInAction(() => {
          this.currentBlock.peerFinanceData = data[0];
          this.currentBlock.peerFinanceData.rank = '(N/A)';
        });
      })
      .catch((err) => {
        if (err.name === 'SignatureMismatchError') {
          console.log(err.message);
          return;
        }
        runInAction(() => {
          this.currentBlock.peerFinanceData = {};
        });
      });
  }

  @action.bound
  setFocusedRow(value) {
    this.focusedRow = value;
  }

  @action.bound
  toggleHistoryExpanded() {
    this.historyExpanded = !this.historyExpanded;
  }

  @action.bound
  setFocusedTab(value) {
    if (this.focusedTab === value) return;
    if (
      (this.entityType === 'stocks' && this.currentStock && this.currentStock.financeData) ||
      (this.entityType === 'blocks' && this.currentBlock && this.currentBlock.financeData)
    ) {
      this.focusedTab = value;
      this.tabLoading = true;
      this.getFinanceData().then(() => {
        this.tabLoading = false;
        this.displayedTab = this.focusedTab;
      });
    }
  }

  @action.bound
  setFocusedBlock(blockCode) {
    let replace = false;
    if (this.focusedBlockCode == null || this.focusedBlockCode === blockCode) {
      replace = true;
    }
    // refetch related stocks if block code changes
    if (this.focusedBlockCode !== blockCode) {
      this.focusedBlockCode = blockCode;
      this.blockLoading = true;
      this.currentBlock =
        this.relatedBlocks.filter((block) => block.code === blockCode)[0] ||
        new BlockModel({
          code: null,
          name: '无板块信息',
        });
      this.getRelatedStocks({ blockCode });
      this.getBlockSummary({ blockCode });
    }
    // otherwise fix url
    const targetUrl = `/stock-app/stocks/${this.currentStock.symbol}/compare/${blockCode}`;
    if (replace) {
      history.replace(targetUrl);
      return;
    }
    history.push(targetUrl);
  }

  @action.bound
  setQuickSearchField(content) {
    this.quickSearchField = content;
  }

  @action.bound
  dispose(reason = null) {
    console.log(`Disposing StockApp Store. Reason: ${reason}`);
    this.entityType = null;
    this.currentBlock = null;
    this.currentStock = null;
    this.relatedBlocks = null;
    this.relatedStocks = null;
    this.focusedBlockCode = null;
    this.focusedRow = null;
    this.historyExpanded = false;
    this.focusedTab = 0;
    this.quickSearchField = '';
    this.quickSearchValue = null;
    this.signatureStore = new SignatureStore();
    this.articleStore = new ArticleStore();
    this.entitySearchStore = new EntitySearchStore(this);
  }

  @action.bound
  initiateStores() {
    this.blockGraphStore = new BlockGraphStore(this);
    this.signatureStore = new SignatureStore();
    this.articleStore = new ArticleStore();
    this.statementSearchStore = new StatementSearchStore();
    this.entitySearchStore = new EntitySearchStore(this);
  }

  constructor() {
    this.apis = {
      blockInfoApi: blockInfoApiInstance,
      blockSummaryLatest: blockSummaryLatestApiInstance,
      stockInfoApi: stockInfoApiInstance,
      financeRatioApi: financeRatioApiInstance,
      relatedIndustriesApi: relatedIndustriesApiInstance,
      relatedStocks: relatedStocksApiInstance,
      businessAnalysis: businessAnalysisApiInstance,
      wechatRecommendation: wechatRecommendationApiInstance,
      statementSearch: statementSearchV2ApiInstance,
      valuation: valuationApiInstance,
      newsEvent: newsEventApiInstance,
      relatedBlocks: relatedBlocksApiInstance,
      blockSecurities: blockSecuritiesApiInstance,
      securityBlocksV2: securityBlocksV2ApiInstance,
      blockSecuritiesV2: blockSecuritiesV2ApiInstance,
      graphCrawler: graphCrawlerApiInstance,
      topicKeyword: topicKeywordApiInstance,
    };
    this.initiateStores();
  }
}

const storeInstance = new Store();

const storeContext = React.createContext(null);

export const StoreProvider = ({ children }) => {
  return <storeContext.Provider value={storeInstance}>{children}</storeContext.Provider>;
};

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) throw new Error('useStore must be used within a StoreProvider.');
  return store;
};

export default Store;
