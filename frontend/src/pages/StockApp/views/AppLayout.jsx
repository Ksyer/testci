import React from 'react';
import { observer } from 'mobx-react';
import {
  Button,
  ButtonGroup,
  Breadcrumbs,
  Link as MuiLink,
  Grid,
  Typography,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

import Layout from '../../../components/Layout/Layout';
import useStyles from '../useStyles';
import { stockAppList, stockAppNameMap } from '../../../config';
import EntitySearch from './components/EntitySearch';

const blockDisabledApp = ['business-analysis', 'events'];

const AppLayout = observer(({ children, entityId, appId, store }) => {
  const classes = useStyles();
  const history = useHistory();
  const { entityType, currentStock, currentBlock, entitySearchStore } = store;
  let entityName = null;
  let entityCode = null;
  let entitySymbol = null;
  if (entityType === 'stocks') {
    entityName = currentStock ? currentStock.displayName : null;
    entityCode = currentStock ? currentStock.code : null;
    entitySymbol = currentStock ? currentStock.symbol : null;
  }
  if (entityType === 'blocks') {
    entityName = currentBlock ? currentBlock.name : null;
    entityCode = currentBlock ? currentBlock.code : null;
    entitySymbol = currentBlock ? currentBlock.code : null;
  }
  return (
    <Layout>
      <Helmet>
        <title>{`${entityName || ''}-${entityCode || ''} 股票分析`}</title>
      </Helmet>
      <Grid container spacing={3} className={classes.wrap}>
        <Grid item xs={12}>
          <Grid container justify="space-between" alignItems="flex-end">
            <Grid item xs={3} className={classes.centerText}>
              <Breadcrumbs>
                {/* <MuiLink>综合股票分析</MuiLink> */}
                {entityName ? (
                  <MuiLink href={entitySymbol ? `/stock-app/${entityType}/${entitySymbol}` : null}>
                    {entityName} {entityCode}
                  </MuiLink>
                ) : (
                  <Skeleton className={classes.inlineSkeleton}>
                    <Typography>股票名称 123456</Typography>
                  </Skeleton>
                )}
                <MuiLink
                  href={entitySymbol ? `/stock-app/${entityType}/${entitySymbol}/${appId}` : null}
                >
                  {stockAppNameMap[appId]}
                </MuiLink>
              </Breadcrumbs>
            </Grid>
            <Grid item xs={9}>
              <EntitySearch store={entitySearchStore} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container justify="space-between" alignItems="flex-end">
            <Grid item xs={6}>
              {entityName ? (
                <>
                  <Typography variant="h5" display="inline">
                    {`${entityName} `}
                  </Typography>
                  <Typography variant="subtitle1" display="inline" className={classes.subtitle}>
                    {entityCode}
                  </Typography>
                </>
              ) : (
                <Skeleton>
                  <Typography variant="h5">股票名称 123456</Typography>
                </Skeleton>
              )}
            </Grid>
            <Grid item xs={6}>
              <Grid container justify="flex-end">
                <ButtonGroup>
                  {stockAppList.map((key) => (
                    <Button
                      key={key}
                      onClick={(e) => {
                        e.preventDefault();
                        history.push(`/stock-app/${entityType}/${entityId}/${key}`);
                      }}
                      variant={appId === key ? 'contained' : 'outlined'}
                      color={appId === key ? 'primary' : 'default'}
                      className={classes.appNameButton}
                      disabled={entityType === 'blocks' && blockDisabledApp.includes(key)}
                      component="a"
                      href={`/stock-app/${entityType}/${entityId}/${key}`}
                    >
                      {stockAppNameMap[key]}
                    </Button>
                  ))}
                </ButtonGroup>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          {children}
        </Grid>
      </Grid>
    </Layout>
  );
});

export default AppLayout;
