import React, { useEffect, useRef } from 'react';
import { observer } from 'mobx-react';
import {
  Divider,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { Route, useParams, useRouteMatch } from 'react-router-dom';
import { Skeleton } from '@material-ui/lab';

import cellFormatter from '../../../utils/cellFormatter';
import { stockCompareConfig } from '../../../consts/displayConfig';
import useStyles from '../useStyles';
import { useStore } from '../stores/store';
import LinkTableCell from './components/LinkTableCell';

const SquarePaper = (props) => <Paper square {...props} />;

const BlockCompare = observer(() => {
  const { path } = useRouteMatch();
  return <Route path={`${path}`} component={BlockCompareInner} />;
});

const BlockCompareInner = observer(() => {
  const store = useStore();
  const { cols, colNameMap, defaultFormatter, colFormatterMap } = stockCompareConfig;
  const classes = useStyles();
  const {
    currentBlock,
    relatedStocks,
    blockLoading,
    navigate,
    makeLink,
    blockGraphStore: { setGraphElement, drawGraph, graphInfo, dispose, handleResize },
  } = store;
  const dataReady = currentBlock != null && relatedStocks != null;
  const graphElement = useRef(null);
  useEffect(() => {
    if (currentBlock == null) return;
    setGraphElement(graphElement.current);
    const preDrawFn = () => {
      graphElement.current.classList.add(classes.graphInnerInit);
      graphElement.current.classList.remove(classes.graphInnerFinal);
    };
    drawGraph(preDrawFn).then(() => {
      graphElement.current.classList.remove(classes.graphInnerInit);
      graphElement.current.classList.add(classes.graphInnerFinal);
      handleResize();
    });
    return dispose;
  }, [currentBlock]);
  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  return (
    <Grid container spacing={3}>
      {/* WIP: Graph Crawler */}
      <Grid item xs={12}>
        <Divider />
      </Grid>
      <Grid item xs={12}>
        <Typography variant="body1">行业关系图</Typography>
      </Grid>
      <Grid item xs={12}>
        <div className={classes.graphFrame}>
          <div className={classes.graphInfo}>
            <Typography>{graphInfo}</Typography>
          </div>
          <div ref={graphElement} className={classes.graphInnerInit} />
        </div>
      </Grid>
      <Grid item xs={12}>
        <Divider />
      </Grid>
      {/* Focus Table */}
      <Grid item xs={12}>
        <TableContainer component={SquarePaper}>
          <Table size="small" className={classes.tableCompact}>
            <TableHead>
              <TableRow>
                <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                  <Typography variant="body1">行业基线</Typography>
                </TableCell>
              </TableRow>
              <TableRow className={classes.keyRow}>
                {cols.map((key) => (
                  <TableCell variant="head" key={key}>
                    {colNameMap[key] || key}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {dataReady && (
              <>
                <TableBody>
                  <TableRow className={`${classes.row} ${classes.rowImportant}`}>
                    {cols.map((key) => (
                      <LinkTableCell
                        key={key}
                        className={`${blockLoading ? classes.loadingCell : ''} ${
                          classes.clickable
                        }`}
                        href={makeLink(currentBlock.code, 'blocks')}
                      >
                        {/* {key === 'display_name' ? currentBlock.name : '(data)'} */}
                        {currentBlock && currentBlock.peerFinanceData
                          ? cellFormatter(
                              currentBlock.peerFinanceData[key],
                              colFormatterMap[key] || defaultFormatter
                            )
                          : '(loading...)'}
                      </LinkTableCell>
                    ))}
                  </TableRow>
                  {/* seperator */}
                  <TableRow>
                    <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                      <Typography variant="body1">相关股票</Typography>
                    </TableCell>
                  </TableRow>
                  {relatedStocks.map((obj) => (
                    <TableRow
                      key={obj.code + obj.peerFinanceData.rank}
                      className={`${classes.row} ${classes.clickable}`}
                      onClick={(e) => {
                        e.preventDefault();
                        navigate(obj.symbol, 'stocks', 'compare');
                      }}
                    >
                      {cols.map((key) => (
                        <LinkTableCell
                          key={key}
                          className={blockLoading ? classes.loadingCell : ''}
                          href={makeLink(obj.symbol, 'stocks', 'compare')}
                        >
                          {cellFormatter(
                            obj.peerFinanceData[key],
                            colFormatterMap[key] || defaultFormatter
                          )}
                        </LinkTableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </>
            )}
            {!dataReady && (
              <>
                <TableBody>
                  <TableRow className={classes.rowImportant}>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  {/* seperator */}
                  <TableRow>
                    <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                      <Typography variant="body1">同类排名</Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                </TableBody>
              </>
            )}
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
});

export default BlockCompare;
