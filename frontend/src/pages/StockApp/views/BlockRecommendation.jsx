import React from 'react';
import { Grid, Button, FormGroup, Typography } from '@material-ui/core';
import { observer } from 'mobx-react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import useStyle from '../useStyles';
import { blockRecommendationConfig } from '../../../consts/displayConfig';
import ArticleTable from '../../ArticleList/views/ArticleTable';
import DateTimeField from '../../StatementSearch/components/DateTimeField';
import CustomTextField from '../../StatementSearch/components/CustomTextField';
import AutoCompleteField from '../../StatementSearch/components/AutoCompleteField';
import CheckboxWithLabel from '../../StatementSearch/components/CheckboxWithLabel';

const SearchSection = observer((props) => {
  const { fieldState, getValue, setValue, getError, changed, handleFormSubmit } = props;
  const classes = useStyle();
  return (
    <Grid container>
      <Grid item xs={12}>
        <form onSubmit={handleFormSubmit}>
          <Grid container spacing={2}>
            {/* article author search field */}
            <Grid item xs={4}>
              <CustomTextField
                name="authorLike"
                displayName="文章作者"
                getValue={getValue}
                setValue={setValue}
              />
            </Grid>
            {/* article title search field */}
            <Grid item xs={4}>
              <CustomTextField
                name="titleLike"
                displayName="文章标题"
                getValue={getValue}
                setValue={setValue}
              />
            </Grid>
            {/* article statements search field */}
            <Grid item xs={4}>
              <CustomTextField
                name="statementsLike"
                displayName="段落内容"
                getValue={getValue}
                setValue={setValue}
              />
            </Grid>
            {/* topic search field */}
            <Grid item xs={4}>
              <AutoCompleteField displayName="相关话题" store={fieldState.relatedTopic} />
            </Grid>
            {/* date time selectors */}
            <Grid item xs={4}>
              <Grid container justify="flex-end" alignContent="center" spacing={2}>
                <Grid item xs={6}>
                  <DateTimeField
                    displayName="开始日期"
                    name="fromDate"
                    getValue={getValue}
                    getError={getError}
                    setValue={setValue}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DateTimeField
                    displayName="开始日期"
                    name="toDate"
                    getValue={getValue}
                    getError={getError}
                    setValue={setValue}
                  />
                </Grid>
              </Grid>
            </Grid>
            {/* source selector */}
            <Grid item xs={4}>
              <FormGroup row>
                <CheckboxWithLabel
                  name="sourceReport"
                  displayName="研报"
                  getValue={getValue}
                  setValue={setValue}
                />
                <CheckboxWithLabel
                  name="sourceWechat"
                  displayName="微信"
                  getValue={getValue}
                  setValue={setValue}
                />
                <CheckboxWithLabel
                  name="sourceXueqiu"
                  displayName="雪球"
                  getValue={getValue}
                  setValue={setValue}
                />
              </FormGroup>
            </Grid>
            {/* empty space */}
            <Grid container item xs={4} alignContent="center" style={{ padding: '0 8px' }}>
              {fieldState.relatedTopic.value != null && (
                <Typography color="primary" className={classes.centerText}>
                  {`匹配话题： ${fieldState.relatedTopic.value.topic}`}
                </Typography>
              )}
            </Grid>
            <Grid item xs={4} />
            {/* submit button */}
            <Grid
              container
              item
              xs={4}
              justify="flex-end"
              alignContent="center"
              style={{ padding: '0 8px' }}
            >
              <Typography color="error" className={classes.centerText}>
                {changed ? '条件已改变，请重新提交表单' : ''}
              </Typography>
              <div style={{ width: '1rem' }} />
              <Grid item style={{ alignSelf: 'center' }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.searchButton}
                  type="submit"
                >
                  筛选
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Grid>
  );
});

const BlockRecommendation = observer(({ store }) => {
  const {
    statementSearchStore: {
      handleFormSubmit,
      filterStore: { fieldState, changed, setValue, getValue, getError },
    },
  } = store;
  return (
    <Grid container spacing={3} direction="column">
      <Grid item xs={12}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <SearchSection
            fieldState={fieldState}
            getValue={getValue}
            setValue={setValue}
            getError={getError}
            changed={changed}
            handleFormSubmit={handleFormSubmit}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item xs={12}>
        <ArticleTable
          title="板块段落列表"
          store={store.statementSearchStore}
          config={blockRecommendationConfig}
        />
      </Grid>
    </Grid>
  );
});

export default BlockRecommendation;
