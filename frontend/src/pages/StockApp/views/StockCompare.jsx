import React, { useEffect, useRef, useState } from 'react';
import { observer } from 'mobx-react';
import {
  Divider,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Chip,
} from '@material-ui/core';
import { Route, useParams, useRouteMatch } from 'react-router-dom';
import { Skeleton } from '@material-ui/lab';

import cellFormatter from '../../../utils/cellFormatter';
import { stockCompareConfig } from '../../../consts/displayConfig';
import useStyles from '../useStyles';
import { useStore } from '../stores/store';
import LinkTableCell from './components/LinkTableCell';

const SquarePaper = (props) => <Paper square {...props} />;

const StockCompare = observer(() => {
  const { path } = useRouteMatch();
  return <Route path={`${path}/:blockId?`} component={StockCompareInner} />;
});

const StockCompareInner = observer(() => {
  const { cols, colNameMap, defaultFormatter, colFormatterMap } = stockCompareConfig;
  const { blockId } = useParams();
  const store = useStore();
  const {
    relatedBlocks,
    defaultBlockCode,
    focusedBlockCode,
    setFocusedBlock,
    relatedStocks,
    currentStock,
    currentBlock,
    blockLoading,
    navigate,
    makeLink,
    blockGraphStore: { setGraphElement, drawGraph, graphInfo, dispose, handleResize },
  } = store;
  const dataReady = currentStock != null && blockId === focusedBlockCode && relatedStocks != null;
  const classes = useStyles();
  const graphElement = useRef(null);
  useEffect(() => {
    // redirect to proper blockId
    if (currentStock != null && relatedBlocks != null) {
      if (blockId == null) {
        setFocusedBlock(defaultBlockCode);
      }
      if (blockId != null && blockId != focusedBlockCode) {
        setFocusedBlock(blockId);
      }
    }
  }, [currentStock, relatedBlocks, blockId, focusedBlockCode]);
  useEffect(() => {
    if (focusedBlockCode == null) return;
    setGraphElement(graphElement.current);
    const preDrawFn = () => {
      graphElement.current.classList.add(classes.graphInnerInit);
      graphElement.current.classList.remove(classes.graphInnerFinal);
    };
    drawGraph(preDrawFn).then(() => {
      graphElement.current.classList.add(classes.graphInnerFinal);
      graphElement.current.classList.remove(classes.graphInnerInit);
      handleResize();
    });
    return dispose;
  }, [focusedBlockCode]);
  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  return (
    <Grid container spacing={3}>
      {/* Divider */}
      <Grid item xs={12}>
        <Divider />
      </Grid>
      {/* topic & tab selector */}
      <Grid item xs={12}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography>同行业</Typography>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.graphFrame}>
              <div className={classes.graphInfo}>
                <Typography>{graphInfo}</Typography>
              </div>
              <div ref={graphElement} className={classes.graphInnerInit} />
            </div>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              {relatedBlocks &&
                relatedBlocks
                  .filter((block) => block.subType === 'industry')
                  .map(({ code, name, category }) => (
                    <Grid item key={code + category}>
                      <Chip
                        variant={focusedBlockCode === code ? 'default' : 'outlined'}
                        color="primary"
                        label={`${name} [${category}]`}
                        disabled={blockLoading}
                        onClick={(e) => {
                          e.preventDefault();
                          setFocusedBlock(code);
                        }}
                        component="a"
                        href={`/stock-app/stocks/${currentStock.symbol}/compare/${code}`}
                      />
                    </Grid>
                  ))}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Divider />
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography>同概念</Typography>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              {relatedBlocks &&
                relatedBlocks
                  .filter((block) => block.subType === 'concept')
                  .map(({ code, name, category }) => (
                    <Grid item key={code + category}>
                      <Chip
                        variant={focusedBlockCode === code ? 'default' : 'outlined'}
                        color="secondary"
                        label={name}
                        disabled={blockLoading}
                        onClick={(e) => {
                          e.preventDefault();
                          setFocusedBlock(code);
                        }}
                        href={`/stock-app/stocks/${currentStock.symbol}/compare/${code}`}
                      />
                    </Grid>
                  ))}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {/* Divider */}
      <Grid item xs={12}>
        <Divider />
      </Grid>
      {/* Focus Table */}
      <Grid item xs={12}>
        <TableContainer component={SquarePaper}>
          <Table size="small" className={classes.tableCompact}>
            <TableHead>
              <TableRow>
                <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                  <Typography variant="body1">当前股票 / 同类均值</Typography>
                </TableCell>
              </TableRow>
              <TableRow className={`${classes.keyRow} ${classes.clickable}`}>
                {cols.map((key) => (
                  <TableCell variant="head" key={key}>
                    {colNameMap[key] || key}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {dataReady && (
              <>
                <TableBody>
                  <TableRow className={`${classes.rowImportant} ${classes.clickable}`}>
                    {currentStock.peerFinanceData ? (
                      cols.map((key) => (
                        <LinkTableCell
                          key={key}
                          className={blockLoading ? classes.loadingCell : ''}
                          href={makeLink(currentStock.symbol, 'stocks', 'compare')}
                        >
                          {cellFormatter(
                            currentStock.peerFinanceData[key],
                            colFormatterMap[key] || defaultFormatter
                          )}
                        </LinkTableCell>
                      ))
                    ) : (
                      <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                        <Typography variant="body1">同类排名</Typography>
                      </TableCell>
                    )}
                  </TableRow>
                  <TableRow className={classes.row}>
                    {cols.map((key) => (
                      <LinkTableCell
                        key={key}
                        className={`${blockLoading ? classes.loadingCell : ''} ${
                          classes.clickable
                        }`}
                        onClick={() => navigate(focusedBlockCode, 'blocks')}
                        href={makeLink(focusedBlockCode, 'blocks')}
                      >
                        {currentBlock && currentBlock.peerFinanceData
                          ? cellFormatter(
                              currentBlock.peerFinanceData[key],
                              colFormatterMap[key] || defaultFormatter
                            )
                          : '(loading...)'}
                      </LinkTableCell>
                    ))}
                  </TableRow>
                  {/* seperator */}
                  <TableRow>
                    <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                      <Typography variant="body1">同类排名</Typography>
                    </TableCell>
                  </TableRow>
                  {relatedStocks.map((obj) => (
                    <TableRow
                      key={obj.code + obj.peerFinanceData.rank}
                      className={
                        obj.code === currentStock.code
                          ? classes.rowImportant
                          : '' + ` ${classes.row} ${classes.clickable}`
                      }
                      onClick={() => {
                        if (obj.code !== currentStock.code) {
                          navigate(obj.symbol, 'stocks', 'compare');
                        }
                      }}
                    >
                      {cols.map((key) => (
                        <LinkTableCell
                          key={key}
                          className={blockLoading ? classes.loadingCell : ''}
                          href={makeLink(obj.symbol, 'stocks', 'compare')}
                        >
                          {cellFormatter(
                            obj.peerFinanceData[key],
                            colFormatterMap[key] || defaultFormatter
                          )}
                        </LinkTableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              </>
            )}
            {!dataReady && (
              <>
                <TableBody>
                  <TableRow className={classes.rowImportant}>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  {/* seperator */}
                  <TableRow>
                    <TableCell colSpan={cols.length} className={classes.seperatorCell}>
                      <Typography variant="body1">同类排名</Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={cols.length}>
                      <Skeleton />
                    </TableCell>
                  </TableRow>
                </TableBody>
              </>
            )}
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
});

export default StockCompare;
