import React, { useState } from 'react';
import {
  Collapse,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { ExpandMore, ExpandLess } from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import { observer } from 'mobx-react';

import cellFormatter from '../../../utils/cellFormatter';
import { newsEventConfig } from '../../../consts/displayConfig';
import useStyles from '../useStyles';

import VisualChart from './components/VisualChart';

const SquarePaper = (props) => <Paper square {...props} />;

const GraphSection = observer((props) => {
  const { symbol, disabled } = props;
  const loadingMessage = disabled ? '图表不可用' : '图表加载中...';
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const toggleOpen = () => {
    setOpen(!open);
  };
  return (
    <Grid container component={SquarePaper}>
      {/* Graph Title */}
      <Grid item xs={12}>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          className={`${classes.row} ${classes.clickable} ${classes.fakeRow}`}
          style={{ borderBottom: '1px solid #c0c0c0' }}
          onClick={toggleOpen}
        >
          <Grid item xs={9}>
            <Typography variant="body1">价格图</Typography>
          </Grid>
          <Grid item>
            {open ? (
              <ExpandLess style={{ fontSize: 20 }} />
            ) : (
              <ExpandMore style={{ fontSize: 20 }} />
            )}
          </Grid>
        </Grid>
      </Grid>
      {/* Graph Frame */}
      <Grid
        item
        xs={12}
        className={open ? classes.graphFrame : `${classes.grpahFrame} ${classes.graphFrameHidden}`}
      >
        {symbol ? (
          <VisualChart symbol={symbol} annotations />
        ) : (
          <div className={classes.graphInner}>{loadingMessage}</div>
        )}
      </Grid>
    </Grid>
  );
});

const CollapseTable = observer(
  ({
    classes,
    open,
    toggleOpen,
    title,
    data,
    cols,
    colNameMap,
    colFormatterMap,
    defaultFormatter,
    keyFunc,
  }) => (
    <TableContainer component={SquarePaper}>
      <Grid
        container
        justify="space-between"
        alignItems="center"
        className={`${classes.row} ${classes.clickable} ${classes.fakeRow}`}
        onClick={toggleOpen}
      >
        <Grid item xs={9}>
          <Typography variant="body1">{title}</Typography>
        </Grid>
        <Grid item>
          {open ? <ExpandLess style={{ fontSize: 20 }} /> : <ExpandMore style={{ fontSize: 20 }} />}
        </Grid>
      </Grid>
      <Collapse in={open}>
        <Table size="small" className={classes.tableCompact}>
          <TableHead>
            <TableRow className={classes.keyRow}>
              {cols.map((colKey) => (
                <TableCell key={colKey}>{colNameMap[colKey]}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {/* Real data */}
            {data != null &&
              data.map((obj) => (
                <TableRow key={keyFunc(obj)}>
                  {cols.map((colKey) => (
                    <TableCell
                      key={colKey}
                      className={colKey === 'content' ? classes.fixedCell20 : ''}
                    >
                      {cellFormatter(obj[colKey], colFormatterMap[colKey] || defaultFormatter)}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            {/* Skeleton */}
            {data == null && (
              <TableRow>
                <TableCell colSpan={cols.length}>
                  <Skeleton />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </Collapse>
    </TableContainer>
  )
);

const StockEvents = observer(({ store }) => {
  // const { data } = mockData;
  const newsEventData = store.currentStock ? store.currentStock.newsEventData : null;
  const classes = useStyles();
  const [newsEventOpen, setNewsEventOpen] = useState(true);
  return (
    <Grid container spacing={3}>
      {/* Price Graph */}
      <Grid item xs={12}>
        <GraphSection
          symbol={store.currentStock && store.currentStock.symbol}
          disabled={store.entityType === 'blocks'}
        />
      </Grid>
      <Grid item xs={12}>
        <CollapseTable
          classes={classes}
          open={newsEventOpen}
          toggleOpen={() => {
            setNewsEventOpen(!newsEventOpen);
          }}
          title="重点事件"
          data={newsEventData}
          cols={newsEventConfig.cols}
          colNameMap={newsEventConfig.colNameMap}
          colFormatterMap={newsEventConfig.colFormatterMap}
          defaultFormatter={newsEventConfig.defaultFormatter}
          keyFunc={(obj) => obj.breaktime}
        />
      </Grid>
    </Grid>
  );
});

export default StockEvents;
