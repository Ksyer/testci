import React, { useState, useCallback, PureComponent } from 'react';
import { observer } from 'mobx-react';
import { Divider } from '@material-ui/core';
import {
  Grid,
  Paper,
  Button,
  Collapse,
  Tab,
  Tabs,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { ExpandMore, ExpandLess } from '@material-ui/icons';

import cellFormatter from '../../../utils/cellFormatter';
import { stockInfoConfig, businessDataConfig } from '../../../consts/displayConfig';
import useStyles from '../useStyles';
import VisualChart from './components/VisualChart';
import EchartsReact from '../../../utils/echarts/EchartsReact';

// const { rows, rowNameMap, keyRow } = financeRatioConfig;
const SquarePaper = (props) => <Paper square {...props} />;

const GraphSection = observer((props) => {
  const { symbol, disabled } = props;
  const loadingMessage = disabled ? '图表不可用' : '图表加载中...';
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const toggleOpen = () => {
    setOpen(!open);
  };
  return (
    <Grid container component={SquarePaper}>
      {/* Graph Title */}
      <Grid item xs={12}>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          className={`${classes.row} ${classes.clickable} ${classes.fakeRow}`}
          style={{ borderBottom: '1px solid #c0c0c0' }}
          onClick={toggleOpen}
        >
          <Grid item xs={9}>
            <Typography variant="body1">价格图</Typography>
          </Grid>
          <Grid item>
            {open ? (
              <ExpandLess style={{ fontSize: 20 }} />
            ) : (
              <ExpandMore style={{ fontSize: 20 }} />
            )}
          </Grid>
        </Grid>
      </Grid>
      {/* Graph Frame */}
      <Grid
        item
        xs={12}
        className={open ? classes.graphFrame : `${classes.grpahFrame} ${classes.graphFrameHidden}`}
      >
        {symbol ? (
          <VisualChart symbol={symbol} />
        ) : (
          <div className={classes.graphInner}>{loadingMessage}</div>
        )}
      </Grid>
    </Grid>
  );
});

const ReportGraph = observer(({ index, report, businessData }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12}>
      <Divider className={classes.divider} />
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="body1" gutterBottom>
            报告期: {businessData.dateList[index]}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          {report.cp != null ? (
            <>
              <EchartsReact option={report.cp} />
              <Typography
                variant="body1"
                className={classes.typography}
                display="block"
                color="textSecondary"
                gutterBottom
              >
                按产品分类
              </Typography>
            </>
          ) : (
            <>
              <Typography variant="caption" display="block" color="textSecondary" gutterBottom>
                该报告期没有按产品分类数据
              </Typography>
            </>
          )}
        </Grid>
        <Grid item xs={12} sm={6}>
          {report.qy != null ? (
            <>
              <EchartsReact option={report.qy} />
              <Typography
                variant="subtitle2"
                className={classes.typography}
                display="block"
                color="textSecondary"
                gutterBottom
              >
                按产品分类
              </Typography>
            </>
          ) : (
            <>
              <Typography variant="caption" display="block" color="textSecondary" gutterBottom>
                该报告期没有按区域分类数据
              </Typography>
            </>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
});

// const { rows, rowNameMap, keyRow } = financeRatioConfig;

const StockInfo = observer(({ store }) => {
  const {
    currentStock,
    focusedRow,
    setFocusedRow,
    historyExpanded,
    // setHistoryExpanded,
    toggleHistoryExpanded,
    focusedTab,
    displayedTab,
    setFocusedTab,
    tabLoading,
  } = store;
  let data = null;
  if (store.entityType === 'stocks') {
    if (store.currentStock && store.currentStock.financeData) {
      data = store.currentStock.financeData;
    }
  } else if (store.entityType === 'blocks') {
    if (store.currentBlock && store.currentBlock.financeData) {
      data = store.currentBlock.financeData;
    }
  }
  const tableConfig =
    displayedTab == 2 ? stockInfoConfig.by_quarter : stockInfoConfig.by_report_or_year;
  const { rows, rowNameMap, keyRow, defaultFormatter, rowFormatterMap } = tableConfig;
  const dataReady = data != null;
  let businessData = null;
  if (store.currentStock && store.currentStock.businessData)
    businessData = store.currentStock.businessData;
  let dates = ['加载中...'];
  if (businessData) dates = businessData.dateList;
  const classes = useStyles();

  function report_2_chart(obj) {
    var cpList = [];
    obj.map(function (row) {
      let cpdata = {
        name: [row.zygc + '：' + row.srbl],
        value: parseFloat(row.srbl.slice(0, -1)),
        info: '主营：' + row.zysr + '</br> ' + '毛利：' + row.mll,
      };
      cpList.push(cpdata);
    });
    let pieData = {
      legend: {
        left: 'left',
        orient: 'vertical',
      },
      tooltip: {
        trigger: 'item',
        formatter: function (obj) {
          return obj['data']['info'];
        },
      },
      series: [
        {
          type: 'pie',
          data: cpList,
          radius: '65%',
          itemStyle: {
            normal: {
              label: {
                textStyle: {
                  fontSize: 11,
                },
              },
            },
          },
        },
      ],
    };
    return pieData;
  }
  var historyList = [];
  if (businessData) {
    let dateStrList = businessData.dateList;
    dateStrList.forEach((dateStr, key) => {
      let d = businessData[dateStr];
      var dateCharts = {
        cp: null,
        qy: null,
      };
      if (typeof d.cp != 'undefined') {
        dateCharts.cp = report_2_chart(d.cp);
      }
      if (typeof d.qy != 'undefined') {
        dateCharts.qy = report_2_chart(d.qy);
      }
      // if (latestReport == null) {
      //   latestReport = dateCharts;
      // } else {
      historyList.push(dateCharts);
    });
  }

  return (
    // Financial Report Section
    <Grid container spacing={3}>
      {/* Price Graph */}
      <Grid item xs={12}>
        <GraphSection
          symbol={currentStock && currentStock.symbol}
          disabled={store.entityType === 'blocks'}
        />
      </Grid>
      <Grid item xs={12}>
        <TableContainer component={SquarePaper}>
          <Tabs className={classes.tabs} value={focusedTab}>
            <Tab
              label="按报告期"
              className={classes.tab}
              disabled={!dataReady}
              onClick={() => setFocusedTab(0)}
            />
            <Tab
              label="按年度"
              className={classes.tab}
              disabled={!dataReady}
              onClick={() => setFocusedTab(1)}
            />
            <Tab
              label="按单季度"
              className={classes.tab}
              disabled={!dataReady}
              onClick={() => setFocusedTab(2)}
            />
          </Tabs>
          <Table size="small" className={classes.tableCompact}>
            <TableHead>
              <TableRow
                className={`${classes.keyRow} ${classes.clickable}`}
                onClick={() => setFocusedRow(null)}
              >
                <TableCell>{rowNameMap[rows[0]]}</TableCell>
                {data
                  ? data.map((obj) => (
                      <TableCell
                        key={obj[keyRow]}
                        className={tabLoading ? classes.loadingCell : ''}
                      >
                        {cellFormatter(obj[rows[0]], rowFormatterMap[rows[0]] || defaultFormatter)}
                      </TableCell>
                    ))
                  : [0, 1, 2, 3, 4, 5, 6, 7, 8].map((key) => (
                      <TableCell key={key}>
                        <Skeleton>
                          <Typography>1,000,000</Typography>
                        </Skeleton>
                      </TableCell>
                    ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.slice(1).map((row) => {
                const rowName = rowNameMap[row] || row;
                return (
                  <TableRow
                    key={rowName}
                    className={`${rowName === focusedRow ? classes.rowFocused : classes.row} ${
                      classes.clickable
                    }`}
                    onClick={() => setFocusedRow(rowName)}
                  >
                    <TableCell variant="head">{rowName}</TableCell>
                    {data ? (
                      data.map((obj) => (
                        <TableCell
                          key={obj[keyRow]}
                          className={tabLoading ? classes.loadingCell : ''}
                        >
                          {cellFormatter(obj[row], rowFormatterMap[row] || defaultFormatter)}
                        </TableCell>
                      ))
                    ) : (
                      <TableCell colSpan={9}>
                        <Skeleton />
                      </TableCell>
                    )}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      {/* Business Composition Graph - Fix */}
      <Grid item xs={12}>
        <Grid container>
          <Grid item xs={12}>
            <Grid container justify="space-between" alignItems="flex-end">
              <Typography variant="body1">主营业务构成图</Typography>
              <Button
                variant="contained"
                onClick={() => toggleHistoryExpanded()}
                style={{ width: '12rem' }}
              >
                {historyExpanded ? '收起' : '显示更多报告期'}
              </Button>
            </Grid>
          </Grid>
          {historyList.slice(0, 1).map((report, key) => (
            <ReportGraph key={key} index={key} report={report} businessData={businessData} />
          ))}
          <Collapse in={historyExpanded}>
            {historyList.slice(1).map((report, key) => (
              <ReportGraph key={key} index={key + 1} report={report} businessData={businessData} />
            ))}
          </Collapse>
        </Grid>
      </Grid>
    </Grid>
  );
});
export default StockInfo;
