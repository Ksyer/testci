import React from 'react';
import { useTheme, withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { observer } from 'mobx-react';

import { stockRecommendationConfig } from '../../../consts/displayConfig';
import ArticleTable from '../../ArticleList/views/ArticleTable';

const StockRecommendation = observer(({ store }) => {
  return (
    <Grid container spacing={3} direction="column">
      <Grid item xs={12}>
        <ArticleTable
          title="股票头条文章"
          store={store.articleStore}
          config={stockRecommendationConfig}
        />
      </Grid>
    </Grid>
  );
});

export default StockRecommendation;
