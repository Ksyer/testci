import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import { useParams } from 'react-router-dom';

import AppLayout from './AppLayout';
import StockInfo from './StockInfo';
import BlockCompare from './BlockCompare';
import StockCompare from './StockCompare';
import StockRecommendation from './StockRecommendation';
import BlockRecommendation from './BlockRecommendation';
import StockEvents from './StockEvents';
import { useStore } from '../stores/store';

const Stocks = observer(() => {
  const { entityId, appId } = useParams();
  const store = useStore();
  useEffect(() => {
    store.currentStock = null;
    store.getCurrentStock({ stockId: entityId });
    return store.dispose;
  }, [entityId]);
  return (
    <AppLayout entityId={entityId} appId={appId} store={store}>
      {appId === 'info' && <StockInfo store={store} />}
      {appId === 'compare' && <StockCompare store={store} />}
      {appId === 'business-analysis' && <BusinessAnalysis store={store} />}
      {appId === 'recommendation' && <StockRecommendation store={store} />}
      {appId === 'events' && <StockEvents store={store} />}
    </AppLayout>
  );
});

const Blocks = observer(() => {
  const { entityId, appId } = useParams();
  const store = useStore();
  useEffect(() => {
    store.currentBlock = null;
    store.getCurrentBlock({ blockId: entityId });
    return store.dispose;
  }, [entityId]);
  return (
    <AppLayout entityId={entityId} appId={appId} store={store}>
      {appId === 'info' && <StockInfo store={store} />}
      {appId === 'compare' && <BlockCompare store={store} />}
      {appId === 'recommendation' && <BlockRecommendation store={store} />}
    </AppLayout>
  );
});

const StockApp = observer(() => {
  const { entityType } = useParams();
  if (entityType === 'stocks') {
    return <Stocks />;
  }
  if (entityType === 'blocks') {
    return <Blocks />;
  }
});

export default StockApp;
