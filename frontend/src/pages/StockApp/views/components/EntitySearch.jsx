import React, { useRef, useCallback } from 'react';
import { observer } from 'mobx-react';
import { Grid } from '@material-ui/core';

import AutoCompleteField from '../../../StatementSearch/components/AutoCompleteField';
import { useEffect } from 'react';

const EntitySearch = observer(({ store }) => {
  const { fieldState, handleSubmit, entityIsBlock } = store;
  const inputRef = useRef(null);
  const handleKeyPress = useCallback((e) => {
    // ignore event if any input has been focused
    if (window.document.activeElement.tagName === 'INPUT') {
      return;
    }
    if (
      (e.key >= 'a' && e.key <= 'z') ||
      (e.key >= 'A' && e.key <= 'Z') ||
      (e.key >= '0' && e.key <= '9') ||
      e.key === ' '
    ) {
      if (e.key === ' ') {
        e.preventDefault();
      }
      if (inputRef.current != null) {
        inputRef.current.focus();
        inputRef.current.select();
      }
    }
  }, []);
  useEffect(() => {
    document.addEventListener('keydown', handleKeyPress);
    return () => {
      document.removeEventListener('keydown', handleKeyPress);
    };
  });
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        console.log('Submitting entity search');
        handleSubmit();
      }}
    >
      <Grid container justify="flex-end">
        <Grid item>
          <AutoCompleteField
            displayName="股票/板块搜索"
            store={fieldState.entity}
            style={{ width: 280 }}
            inputRef={inputRef}
          />
        </Grid>
        <div style={{ padding: '0 8px' }} />
        <Grid item>
          <AutoCompleteField
            displayName="相关话题"
            store={fieldState.topic}
            style={{ width: 280 }}
            disabled={!entityIsBlock}
          />
        </Grid>
        <button type="submit" style={{ display: 'none' }} />
      </Grid>
    </form>
  );
});

export default EntitySearch;
