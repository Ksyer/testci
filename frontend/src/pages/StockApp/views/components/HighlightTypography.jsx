import React from 'react';
import { observer } from 'mobx-react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import makeSegmentsMultiLevel from './makeSegmentsMultiLevel';

/*
<HighlightTypography />

A drop-in replacement for <Typography /> that highlights multiple keywords.

@prop children:String: The text to be displayed
@prop keywords:[String]: The keywords to be highlighted
@prop highlightStyles:Object: Style to be applied to keywords

Limitations:
<HighlightTypography /> only accepts string as chidren.
*/

const highlightBase = (theme) => ({
  fontWeight: 500,
  color: theme.palette.primary.dark,
  paddingTop: '0.2rem',
});

const useStyles = makeStyles((theme) => ({
  default: {},
  highlightLv0: {
    ...highlightBase(theme),
    backgroundColor: '#aff',
  },
  highlightLv1: {
    ...highlightBase(theme),
    backgroundColor: '#ff0',
  },
  highlightLv2: {
    ...highlightBase(theme),
    backgroundColor: '#fc8',
  },
  highlightLv3: {
    ...highlightBase(theme),
    backgroundColor: '#fcd',
  },
}));

const escapeRegExp = (string) => string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

const makeRegExp = (keywords) =>
  new RegExp(
    keywords
      .filter((keyword) => keyword != null && keyword != '')
      .map(escapeRegExp)
      .join('|'),
    'g'
  );

const makeSegments = (content, regKeyword) => [
  String(content).match(regKeyword),
  String(content).split(regKeyword),
];

const HighlightTypographyLegacy = observer(
  ({ children, keywords, highlightStyle = null, ...props }) => {
    const classes = useStyles();
    let _children = children;
    // serialize children if children is not string
    if (typeof children !== 'string') {
      console.warn(
        'Warning: Children of HighlightTypography should be of type string. Casting children to string with JSON.stringify().'
      );
      _children = JSON.stringify(children);
    }
    if (keywords != null && !Array.isArray(keywords)) {
      throw 'Error: Kewords of HighlightTypography must be an array';
    }
    if (
      keywords == null ||
      keywords.filter((keyword) => keyword != null && keyword != '').length === 0
    ) {
      return <Typography {...props}>{_children}</Typography>;
    }
    const regKeyword = makeRegExp(keywords);
    const [matches, others] = makeSegments(_children, regKeyword);
    return (
      <Typography {...props}>
        {others.length > 0 && <span>{others[0]}</span>}
        {others.length > 1 &&
          others.slice(1).map((other, i) => (
            <React.Fragment key={i}>
              <span style={highlightStyle} className={classes.highlightLv1}>
                {matches[i]}
              </span>
              <span>{other}</span>
            </React.Fragment>
          ))}
      </Typography>
    );
  }
);

const getClassFromLevel = (classes, level) => {
  switch (level) {
    case -1:
      return classes.default;
    case 0:
      return classes.highlightLv0;
    case 1:
      return classes.highlightLv1;
    case 2:
      return classes.highlightLv2;
    case 3:
      return classes.highlightLv3;
  }
};

const HighlightTypographyInner = observer(
  ({ children, multiLevelKeywords, highlightStyle = null, ...props }) => {
    let _children = children;
    // serialize children if children is not string
    if (typeof children !== 'string') {
      console.warn(
        'Warning: Children of HighlightTypography should be of type string. Casting children to string with JSON.stringify().'
      );
      _children = JSON.stringify(children);
    }
    // if no keywords are provided, return a normal Typography
    if (multiLevelKeywords == null) {
      return <Typography {...props}>{_children}</Typography>;
    }
    // assert that multiLevelKeywords is an array
    if (!Array.isArray(multiLevelKeywords)) {
      throw 'Error: Kewords of HighlightTypography must be an array';
    }
    const [result, levelList] = makeSegmentsMultiLevel(_children, multiLevelKeywords);
    const classes = useStyles();
    return (
      <Typography {...props}>
        {result.map((content, i) => (
          <span
            key={`segment-${i}`}
            style={highlightStyle}
            className={getClassFromLevel(classes, levelList[i])}
          >
            {content}
          </span>
        ))}
      </Typography>
    );
  }
);

const HighlightTypographyWrapper = observer((props) => {
  const { keywords, multiLevelKeywords } = props;
  if (keywords != null && multiLevelKeywords != null) {
    throw 'Error: only one of props keywords or multiLevelKeywords should be provided.';
  }
  if (keywords != null) {
    return <HighlightTypographyLegacy {...props} />;
  }
  return <HighlightTypographyInner {...props} />;
});

export default HighlightTypographyWrapper;
