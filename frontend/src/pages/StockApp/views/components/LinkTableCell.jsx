import React from 'react';
import { TableCell } from '@material-ui/core';
import { observer } from 'mobx-react';

import useStyles from '../../useStyles';

const noEvent = (e) => {
  e.preventDefault();
};

const LinkTableCell = observer((props) => {
  const { href, children, ...others } = props;
  const classes = useStyles();
  return (
    <TableCell {...others}>
      <a href={href} onClick={noEvent} className={classes.tableLinkInner}>
        {children}
      </a>
    </TableCell>
  );
});

export default LinkTableCell;
