import React, { useState, useEffect, useRef } from 'react';
import { observer } from 'mobx-react';
import axios from 'axios';

// View component for rendering TradingKing.visual chart.
// Uses iframe for chart integration.
// Renders chart from localhost:58001 when in development mode;
// Renders chart from window.location.host/visual/ when in production mode. (Reverse-Proxy handled by Nginx)

let chartOrigin = `http://${window.location.host}/visual/`;
if (process.env.NODE_ENV === 'development') {
  chartOrigin = process.env.REACT_APP_DEV_VISUAL_ORIGIN;
}
chartOrigin = new URL(chartOrigin);

const chartIdURL = new URL('chart-server/1.1/charts/', chartOrigin);
const userId = 'tradingking';
const clientId = 'tradingking-0';
const configId = 'prod.nginx';

const useVisualChart = (symbol, annotations) => {
  const [chartId, setChartId] = useState(null);
  const symbolRef = useRef();
  useEffect(() => {
    setChartId(null);
    if (annotations !== true) {
      setChartId(0);
      return;
    }
    // store latest symbol to stop outdated responses from being loaded
    symbolRef.current = symbol;
    axios
      .get(chartIdURL.toString(), {
        params: {
          user: userId,
          client: clientId,
          symbol: symbol,
        },
      })
      .then((response) => {
        const { data } = response;
        // if symbol !== latest symbol, discard data.
        if (symbol === symbolRef.current) {
          if (data.data.length > 0) {
            const id = data.data[0].id;
            setChartId(id);
          } else {
            setChartId(0);
          }
        }
      });
  }, [symbol]);
  return chartId;
};

const VisualChartIframe = (props) => {
  const { symbol, chartId } = props;
  if (chartId != null) {
    let chartVisualURL = null;
    // if chartId is not found, render chart by symbol
    // otherwise render chart by chartId
    if (chartId === 0) {
      chartVisualURL = new URL(
        `?config=${configId}&user_id=${userId}$client_id=${clientId}&symbol=cn.${symbol}`,
        chartOrigin
      );
    } else {
      chartVisualURL = new URL(
        `?config=${configId}&user_id=${userId}&client_id=${clientId}&chart=${chartId}`,
        chartOrigin
      );
    }
    return (
      <iframe
        src={chartVisualURL.toString()}
        title={`tradingking-visual-${symbol}`}
        style={{
          position: 'absolute',
          top: -1.5,
          left: -2,
          width: '100%',
          height: '100%',
        }}
      ></iframe>
    );
  }
  return null;
};

const VisualChart = observer((props) => {
  const { symbol, annotations } = props;
  const chartId = useVisualChart(symbol, annotations);
  return <VisualChartIframe symbol={symbol} chartId={chartId} />;
});

export default VisualChart;
