const escapeRegExp = (string) => string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

const makeRegExp = (validKeywords) =>
  new RegExp(
    validKeywords
      .filter((keyword) => keyword != null && keyword != '')
      .map(escapeRegExp)
      .join('|'),
    'g'
  );

// cascadeKeywordList should be in format:
// [['level0', 'lv0keywords'], ['level1', 'lv1Keywords']]
// level -1 means no match
const makeSegmentsMultiLevel = (content, cascadeKeywordList, level = 0) => {
  // ensure type of content is string
  if (typeof content !== typeof String()) {
    throw 'Error: Type of content must be string';
  }
  if (!Array.isArray(cascadeKeywordList)) {
    throw 'Error: Type of regKeywordList must be array';
  }
  // if no content, return no content
  if (content.length === 0) {
    return [[], []];
  }
  // if no keywords left, return no match
  if (cascadeKeywordList.length === 0) {
    return [[content], [-1]];
  }
  console.log(cascadeKeywordList);
  const levelKeywords = cascadeKeywordList[0].filter((keyword) => keyword !== '');
  // validate keywords
  levelKeywords.forEach((keyword) => {
    if (typeof keyword !== typeof String() || keyword === '') {
      throw `Invalid keyword: ${keyword} of type ${typeof keyword}`;
    }
  });
  // if no keywords at current level, drop to next level
  if (levelKeywords.length === 0) {
    return makeSegmentsMultiLevel(content, cascadeKeywordList.slice(1), level + 1);
  }
  const regPattern = makeRegExp(levelKeywords);
  const matches = content.match(regPattern);
  const nonMatches = content.split(regPattern);
  const result = [];
  const levelList = [];
  nonMatches.forEach((nonMatchSegment, i) => {
    // recurse on non-match segments
    const [_result, _levelList] = makeSegmentsMultiLevel(
      nonMatchSegment,
      cascadeKeywordList.slice(1),
      level + 1
    );
    result.push(..._result);
    levelList.push(..._levelList);
    // append match segment
    if (matches != null && i < matches.length) {
      result.push(matches[i]);
      levelList.push(level);
    }
  });
  return [result, levelList];
};

export default makeSegmentsMultiLevel;
