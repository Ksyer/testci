import makeSegmentsMultiLevel from './makeSegmentsMultiLevel';

it('Segments strings with multiple keywords', () => {
  const testString = 'aabbccaabbccaa';
  // single level, no empty segments at each end
  expect(makeSegmentsMultiLevel(testString, [['aa']])).toEqual([
    ['aa', 'bbcc', 'aa', 'bbcc', 'aa'],
    [0, -1, 0, -1, 0],
  ]);
  // multiple level
  expect(makeSegmentsMultiLevel(testString, [['bc'], ['ca', 'cc']])).toEqual([
    ['aab', 'bc', 'ca', 'ab', 'bc', 'ca', 'a'],
    [-1, 0, 1, -1, 0, 1, -1],
  ]);
  expect(makeSegmentsMultiLevel(testString, [['bc', 'ca'], ['cc']])).toEqual([
    ['aab', 'bc', 'ca', 'ab', 'bc', 'ca', 'a'],
    [-1, 0, 0, -1, 0, 0, -1],
  ]);
  // empty level in between
  expect(makeSegmentsMultiLevel(testString, [['aa', 'bb'], [], ['cc']])).toEqual([
    ['aa', 'bb', 'cc', 'aa', 'bb', 'cc', 'aa'],
    [0, 0, 2, 0, 0, 2, 0],
  ]);
  // edge case: no content
  expect(makeSegmentsMultiLevel('', [['bc', 'ca', 'cc']])).toEqual([[], []]);
  // edge case: no keyword
  expect(makeSegmentsMultiLevel(testString, [])).toEqual([['aabbccaabbccaa'], [-1]]);
});
