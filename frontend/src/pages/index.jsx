import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import Layout from '../components/Layout/Layout';
import { Helmet } from 'react-helmet-async';

const IndexPage = () => (
  <Layout>
    <Helmet>
      <title>Index Page</title>
    </Helmet>
    <Grid container direction="column">
      <Link to="/stock-app/stocks/300122">股票分析: 智飞生物 300122</Link>
      <Link to="/stock-articles">头条文章总览</Link>
      <Link to="/admin-tool/article">后台管理工具</Link>
      <Link to="/statement-search">段落高级搜索</Link>
    </Grid>
  </Layout>
);

export default IndexPage;
